import React from 'react';
import {
	ExportOutlined,
	ApartmentOutlined,
	TeamOutlined,
	ProfileOutlined,
	ControlOutlined,
} from '@ant-design/icons';
import { Layout, Menu, Tooltip, Divider, Space } from 'antd';
import { IMainLayoutProps } from '../../model/IMainLayoutProps';
import { Link } from 'react-router-dom';
import './index.scss';

const { SubMenu } = Menu;
const { Sider } = Layout;

interface IMenuItem {
	key: string;
	title: string;
	icon?: React.ReactNode;
	permission?: string;

	children?: IMenuItem[];
}

const menuList: IMenuItem[] = [
	{
		key: '/management',
		icon: <ApartmentOutlined />,
		title: 'Quản lý',
		children: [
			{
				key: '/management/company-type',
				title: 'Loại hình công ty',
				permission: '',
			},
			{
				key: '/management/currency',
				title: 'Loại tiền tệ',
				permission: '',
			},
			{
				key: '/management/account-number',
				title: 'Tài khoản kế toán',
				permission: '',
			},
			{
				key: '/management/tax',
				title: 'Thuế',
				permission: '',
			},
			{
				key: '/management/bank',
				title: 'Ngân hàng',
				permission: '',
			},
		],
	},
	{
		key: '/role-permission',
		icon: <TeamOutlined />,
		title: 'Vai trò',
	},
	{
		key: '/system-constant',
		icon: <ControlOutlined />,
		title: 'Hằng số hệ thống',
	},
];

export const NavigationBar = (props: IMainLayoutProps) => {
	const [collapsed] = React.useState(false);
	const renderMenuItem = () => {
		return menuList.map((item) => {
			if (item['children'] !== undefined) {
				return (
					<SubMenu key={item.key} icon={item.icon} title={item.title}>
						{item['children'].map((child) => {
							return (
								<Menu.Item key={child.key}>
									<Link to={child.key}>{child.title}</Link>
								</Menu.Item>
							);
						})}
					</SubMenu>
				);
			}
			return (
				<Menu.Item key={item.key} icon={item.icon}>
					<Link to={item.key}>{item.title}</Link>
				</Menu.Item>
			);
		});
	};
	return (
		<div className="navigation">
			<div className="navigation_header">
				<Link to="/" className="navigation_logo">
					{/* <img className="logo hints" src={logoHeader} alt="logo" height="64" width="128" /> */}
					<span style={{ fontSize: '25px', fontWeight: 900, color: '#606060' }}>PD</span>
					<span style={{ fontSize: '25px', fontWeight: 900, color: '#606060' }}>Accounting</span>
					{/* <img
						className="small-logo hints"
						src="/static/newmedia/mailicon.png"
						height="35"
						width="33"
						alt="small logo"
					/> */}
				</Link>
			</div>
			<Sider
				trigger={null}
				collapsible={true}
				collapsed={collapsed}
				className="navigation_menu_wrapper"
			>
				<Menu theme="light" mode="inline" className="pt-2">
					{renderMenuItem()}
				</Menu>
				<Divider className="m-0" />
				<div className="py-3 px-4 bg-white slider_actions">
					<div className="d-flex justify-content-center">
						<Space align="center" direction="horizontal" size={24}>
							<Tooltip placement="top" title="Trợ giúp">
								<ProfileOutlined />
							</Tooltip>
							<Tooltip placement="top" title="Đăng xuất">
								<ExportOutlined onClick={() => props.actions.handleLogout()} />
							</Tooltip>
						</Space>
					</div>
				</div>
			</Sider>
		</div>
	);
};
