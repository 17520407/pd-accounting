/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IError } from '../../common/interfaces';
import { ISystemConstant } from './model/ISystemConstantState';

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: string;
	};
}

//#region Fetch System Constants IActions
export interface IFetchSystemConstants extends Action {
	readonly type: Keys.FETCH_SYSTEM_CONSTANTS;
}

export interface IFetchSystemConstantsSuccess extends Action {
	readonly type: Keys.FETCH_SYSTEM_CONSTANTS_SUCCESS;
	payload: ISystemConstant;
}

export interface IFetchSystemConstantsFail extends Action {
	readonly type: Keys.FETCH_SYSTEM_CONSTANTS_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Update System Constants IActions
export interface IUpdateSystemConstants extends Action {
	readonly type: Keys.UPDATE_SYSTEM_CONSTANTS;
	payload: ISystemConstant;
}

export interface IUpdateSystemConstantsSuccess extends Action {
	readonly type: Keys.UPDATE_SYSTEM_CONSTANTS_SUCCESS;
	payload: ISystemConstant;
}

export interface IUpdateSystemConstantsFail extends Action {
	readonly type: Keys.UPDATE_SYSTEM_CONSTANTS_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
