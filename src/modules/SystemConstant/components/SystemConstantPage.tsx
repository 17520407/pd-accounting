import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { ISystemConstantProps } from '../model/ISystemConstantProps';
import { CustomBreadCrumb } from '../../../components';
import { UpdateSystemConstantsForm } from './Form';

import './index.scss';
import { Spin } from 'antd';

interface IProps extends RouteComponentProps, ISystemConstantProps {}

export const SystemConstantPage: React.FC<IProps> = ({ actions, store }) => {
	const { isLoading } = store.SystemConstant;

	React.useEffect(() => {
		actions.fetchSystemConstants();
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb title="Hằng số hệ thống" path="/system-constant" routeName="Cấu hình" />
			<div className="box">
				<div className="box_content">
					<Spin spinning={isLoading}>
						<UpdateSystemConstantsForm {...{ actions, store }} />
					</Spin>
				</div>
			</div>
		</React.Fragment>
	);
};
