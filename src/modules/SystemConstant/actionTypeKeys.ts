/**
 * @file  Key of action will be listed here
 */

enum ActionTypeKeys {
	HANDLE_CLEAR = 'LOGIN_PAGE/HANDLE_CLEAR',

	FETCH_SYSTEM_CONSTANTS = 'SYSTEM_CONSTANT/FETCH_SYSTEM_CONSTANTS',
	FETCH_SYSTEM_CONSTANTS_SUCCESS = 'SYSTEM_CONSTANT/FETCH_SYSTEM_CONSTANTS_SUCCESS',
	FETCH_SYSTEM_CONSTANTS_FAIL = 'SYSTEM_CONSTANT/FETCH_SYSTEM_CONSTANTS_FAIL',

	UPDATE_SYSTEM_CONSTANTS = 'SYSTEM_CONSTANT/UPDATE_SYSTEM_CONSTANTS',
	UPDATE_SYSTEM_CONSTANTS_SUCCESS = 'SYSTEM_CONSTANT/UPDATE_SYSTEM_CONSTANTS_SUCCESS',
	UPDATE_SYSTEM_CONSTANTS_FAIL = 'SYSTEM_CONSTANT/UPDATE_SYSTEM_CONSTANTS_FAIL',
}

export default ActionTypeKeys;
