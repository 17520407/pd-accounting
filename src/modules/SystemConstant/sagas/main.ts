import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as SystemConstantApi from '../../../api/systemConst';
import { message } from 'antd';

// Handle Fetch System Constants
function* handleFetchSystemConstants(action: any) {
	try {
		const res = yield call(SystemConstantApi.fetchSystemConst);
		if (res.status === 200) {
			yield delay(200);
			yield put(actions.fetchSystemConstantsSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchSystemConstantsFail(error));
	}
}

// Handle Update System Constants
function* handleUpdateSystemConstants(action: any) {
	try {
		const res = yield call(SystemConstantApi.updateSystemConst, action.payload);
		if (res.status === 200) {
			yield delay(200);
			message.success('Cập nhật hằng số hệ thống thành công', 2);
			yield put(actions.updateSystemConstantsSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.updateSystemConstantsFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchFetchSystemConstants() {
	yield takeEvery(Keys.FETCH_SYSTEM_CONSTANTS, handleFetchSystemConstants);
}
function* watchUpdateSystemConstants() {
	yield takeEvery(Keys.UPDATE_SYSTEM_CONSTANTS, handleUpdateSystemConstants);
}
/*-----------------------------------------------------------------*/
const sagas = [watchFetchSystemConstants, watchUpdateSystemConstants];
export default sagas;
