import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { ISystemConstant } from './model/ISystemConstantState';

//#region Fetch System Constants Actions
export const fetchSystemConstants = (): IActions.IFetchSystemConstants => {
	return {
		type: Keys.FETCH_SYSTEM_CONSTANTS,
	};
};

export const fetchSystemConstantsSuccess = (
	res: ISystemConstant
): IActions.IFetchSystemConstantsSuccess => {
	return {
		type: Keys.FETCH_SYSTEM_CONSTANTS_SUCCESS,
		payload: res,
	};
};

export const fetchSystemConstantsFail = (res: IError[]): IActions.IFetchSystemConstantsFail => {
	return {
		type: Keys.FETCH_SYSTEM_CONSTANTS_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Update System Constants Actions
export const updateSystemConstants = (data: ISystemConstant): IActions.IUpdateSystemConstants => {
	return {
		type: Keys.UPDATE_SYSTEM_CONSTANTS,
		payload: data,
	};
};

export const updateSystemConstantsSuccess = (
	res: ISystemConstant
): IActions.IUpdateSystemConstantsSuccess => {
	return {
		type: Keys.UPDATE_SYSTEM_CONSTANTS_SUCCESS,
		payload: res,
	};
};

export const updateSystemConstantsFail = (res: IError[]): IActions.IUpdateSystemConstantsFail => {
	return {
		type: Keys.UPDATE_SYSTEM_CONSTANTS_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
