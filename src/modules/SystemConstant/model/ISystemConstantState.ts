export interface ISystemConstant {
	userTotalFreeBill: string;
	userTotalFreeCompany: string;
}

export interface ISystemConstantState {
	systemConstants?: ISystemConstant;
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: ISystemConstantState = {
	isLoading: false,
	isProcessing: false,
};
