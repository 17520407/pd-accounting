/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IRole } from '../../common/interfaces';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: number;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: number;
	};
}

export interface IHandleRoleNameChange extends Action {
	readonly type: Keys.HANDLE_ROLE_NAME_CHANGE;
	payload: {
		value: string;
	};
}

export interface IHandleRoleSelect extends Action {
	readonly type: Keys.HANDLE_ROLE_SELECT;
	payload: IRole;
}

export interface IHandlePermissionSwitch extends Action {
	readonly type: Keys.HANDLE_PERMISSION_SWITCH;
	payload: {
		value: { permissionKey: string; _id: string };
		status: boolean;
	};
}

//#region  Get Roles IActions
export interface IGetRoles extends Action {
	readonly type: Keys.GET_ROLES;
	payload: {
		page?: number | 0;
		limit?: number | 10;
	};
}
export interface IGetRolesSuccess extends Action {
	readonly type: Keys.GET_ROLES_SUCCESS;
	payload: any;
}
export interface IGetRolesFail extends Action {
	readonly type: Keys.GET_ROLES_FAIL;
	payload: any;
}
//#endregion

//#region  Post Role IActions
export interface IPostRole extends Action {
	readonly type: Keys.POST_ROLE;
	payload: {
		roleName: string;
		rolePermissions: any;
	};
}
export interface IPostRoleSuccess extends Action {
	readonly type: Keys.POST_ROLE_SUCCESS;
	payload: any;
}
export interface IPostRoleFail extends Action {
	readonly type: Keys.POST_ROLE_FAIL;
	payload: any;
}
//#endregion

//#region Change Role's Status IActions
export interface IChangeRoleStatus extends Action {
	readonly type: Keys.CHANGE_ROLE_STATUS;
	payload: {
		_id: string;
		type: string;
	};
}
export interface IChangeRoleStatusSuccess extends Action {
	readonly type: Keys.CHANGE_ROLE_STATUS_SUCCESS;
}
export interface IChangeRoleStatusFail extends Action {
	readonly type: Keys.CHANGE_ROLE_STATUS_FAIL;
	payload: {
		code: string;
		field: string;
		message: string;
	}[];
}
//#endregion

//#region  Put Role IActions
export interface IPutRole extends Action {
	readonly type: Keys.PUT_ROLE;
	payload: {
		_id: string;
		roleName: string;
		rolePermissions: any;
	};
}
export interface IPutRoleSuccess extends Action {
	readonly type: Keys.PUT_ROLE_SUCCESS;
	payload: IRole;
}
export interface IPutRoleFail extends Action {
	readonly type: Keys.PUT_ROLE_FAIL;
	payload: any;
}
//#endregion

//#region  Delete Role IActions
export interface IDeleteRole extends Action {
	readonly type: Keys.DELETE_ROLE;
	payload: {
		_id: string;
	};
}
export interface IDeleteRoleSuccess extends Action {
	readonly type: Keys.DELETE_ROLE_SUCCESS;
	payload: any;
}
export interface IDeleteRoleFail extends Action {
	readonly type: Keys.DELETE_ROLE_FAIL;
	payload: any;
}
//#endregion

//#region  Get Permissions IActions
export interface IGetPermissions extends Action {
	readonly type: Keys.GET_PERMISSIONS;
}
export interface IGetPermissionsSuccess extends Action {
	readonly type: Keys.GET_PERMISSIONS_SUCCESS;
	payload: any;
}
export interface IGetPermissionsFail extends Action {
	readonly type: Keys.GET_PERMISSIONS_FAIL;
	payload: any;
}
//#endregion
