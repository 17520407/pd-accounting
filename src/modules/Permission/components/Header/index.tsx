import * as React from 'react';
import { IPermissionProps } from '../../model/IPermissionProps';
import { PageHeader, Popconfirm, Button } from 'antd';
import { DeleteOutlined, RetweetOutlined, UnlockOutlined, LockOutlined } from '@ant-design/icons';
import { TextEditable } from '../../../../components';
// import { isGranted } from '../../../../services/checkPermission';

interface IProps extends IPermissionProps {}

export const PermissionHeader: React.FC<IProps> = (props: IProps) => {
	const { roleSelected, isProcessing, isUpdating } = props.store.PermissionPage;

	return (
		<PageHeader
			title={
				<div
					style={{
						display: 'flex',
						paddingTop: 5,
					}}
				>
					<div>Vai trò:</div>
					<div
						style={{
							marginLeft: '1rem',
						}}
					>
						<TextEditable
							text={roleSelected.roleName}
							message="Vui lòng nhập tên danh mục"
							onChange={(e: any) => props.actions.handleRoleNameChange(e)}
						/>
					</div>
				</div>
			}
			extra={[
				<Button
					danger={roleSelected.status === 'enable'}
					key="3"
					type="primary"
					icon={roleSelected.status === 'enable' ? <LockOutlined /> : <UnlockOutlined />}
					loading={isProcessing}
					onClick={() => {
						props.actions.changeRoleStatus({
							_id: roleSelected._id,
							type: roleSelected.status === 'enable' ? 'disable' : 'enable',
						});
					}}
				>
					{roleSelected.status === 'enable' ? ' Tắt vai trò' : 'Kích hoạt vai trò'}
				</Button>,
				<Button
					key="2"
					type="primary"
					icon={<RetweetOutlined />}
					onClick={() =>
						!isProcessing &&
						props.actions.putRole({
							_id: roleSelected._id,
							roleName: roleSelected.roleName,
							rolePermissions: roleSelected.rolePermissions,
						})
					}
					loading={isProcessing}
					disabled={!isUpdating}
				>
					Cập nhật
				</Button>,
				<Popconfirm
					key="1"
					placement="topLeft"
					title="Bạn có chắc chắn xóa vai trò này?"
					okText="Có"
					cancelText="Không"
					onConfirm={() =>
						!isProcessing &&
						props.actions.deleteRole({
							_id: roleSelected._id,
						})
					}
				>
					<Button
						key="1"
						danger={true}
						type="primary"
						icon={<DeleteOutlined />}
						loading={isProcessing}
						// disabled={!isGranted('roleDelete')}
					>
						Xóa
					</Button>
				</Popconfirm>,
			]}
		/>
	);
};
