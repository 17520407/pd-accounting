import IStore from '../../../redux/store/IStore';
import * as permissionActions from '../actions';
import ActionTypes from '../actionTypes';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PermissionPage from './PermissionPage';
import { withRouter } from 'react-router-dom';

function mapStateToProps(store: IStore) {
	return {
		store: store,
	};
}

function mapDispatchToProps(dispatch: Dispatch<ActionTypes>) {
	const allActions = {
		...permissionActions,
	};
	return {
		actions: bindActionCreators(allActions, dispatch),
	};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PermissionPage));
