import * as React from 'react';
import { IPermissionProps } from '../model/IPermissionProps';
import { RouteComponentProps } from 'react-router';
import { Col, Row } from 'antd';
import { PERMISSION_CLEAR } from '../model/IPermissionState';
import { PermissionTable } from './Table';
import { RoleCreateEditForm } from './Form/RoleCreateEditForm';
import { RoleList } from './RoleList';
import { PermissionHeader } from './Header';
import './index.scss';
import { CustomBreadCrumb } from '../../../components';

interface IProps extends RouteComponentProps, IPermissionProps {}

class PermissionPage extends React.Component<IProps> {
	componentDidMount() {
		this.props.actions.getRoles({ page: 0, limit: 10 });
		this.props.actions.getPermissions();
	}
	componentDidUpdate(prevProps: any) {
		const { isRolesLoaded, isPermissionsLoaded, roleRecords } = this.props.store.PermissionPage;
		if (isPermissionsLoaded && isRolesLoaded) {
			if (roleRecords.length > 0) {
				this.props.actions.handleRoleSelect(roleRecords[0]);
			}
			this.props.actions.handleClear(PERMISSION_CLEAR.ROLE_PERMISSIONS_LOADED);
		}
	}

	UNSAFE_componentWillMount() {
		this.props.actions.handleClear(PERMISSION_CLEAR.CLEAR_WHEN_UN_MOUNT);
	}

	public render(): JSX.Element {
		const { roleSelected } = this.props.store.PermissionPage;
		return (
			<React.Fragment>
				<CustomBreadCrumb title="Vai trò" path="/role-permission" routeName="Phân quyền" />
				<Row gutter={16}>
					<Col xl={6} lg={8} md={24} sm={24} xs={24}>
						<div className="box">
							<div className="box_content">
								<RoleList {...this.props} />
							</div>
						</div>
					</Col>

					<Col xl={18} lg={16} md={24} sm={24} xs={24}>
						<div className="box">
							<div className="box_content">
								{roleSelected && <PermissionHeader {...this.props} />}
								<PermissionTable {...this.props} />
							</div>
						</div>
					</Col>
				</Row>
				<RoleCreateEditForm {...this.props} />
			</React.Fragment>
		);
	}
}
export default PermissionPage;
