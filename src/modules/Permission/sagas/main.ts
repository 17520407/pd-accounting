import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { message } from 'antd';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as PermissionApi from '../../../api/permission';

// Handle Get Roles
function* handleGetRoles(action: any) {
	try {
		const res = yield call(PermissionApi.getRoles, action.payload);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.getRolesSuccess(res.data.data));
		} else {
			yield put(actions.getRolesFail(res));
		}
	} catch (error) {
		yield put(actions.getRolesFail(error));
	}
}

// Handle Post Role
function* handlePostRole(action: any) {
	try {
		const res = yield call(PermissionApi.postRole, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Thêm thành công', 0.5);
			yield put(actions.postRoleSuccess(res.data.data));
		} else {
			message.error('Thêm vai trò xảy ra lỗi', 0.5);
			yield put(actions.postRoleFail(res));
		}
	} catch (error) {
		yield put(actions.postRoleFail(error));
	}
}

// Handle Put Role
function* handlePutRole(action: any) {
	try {
		const res = yield call(PermissionApi.putRole, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Cập nhật thành công', 1);
			yield put(actions.putRoleSuccess(res.data.data));
		} else {
			message.error('Cập nhật vai trò xảy ra lỗi', 1);
			yield put(actions.putRoleFail(res));
		}
	} catch (error) {
		yield put(actions.putRoleFail(error));
	}
}

// Handle Change Role Status
function* handleChangeRoleStatus(action: any) {
	try {
		let res = null;
		if (action.payload.type === 'enable') {
			res = yield call(PermissionApi.enableRole, action.payload);
		} else {
			res = yield call(PermissionApi.disableRole, action.payload);
		}
		yield delay(500);
		if (res.status === 200) {
			message.success('Thay đổi trạng thái vai trò thành công', 2);
			yield put(actions.changeRoleStatusSuccess());
		} else {
			yield put(actions.changeRoleStatusFail(res.data.errors));
		}
	} catch (error) {
		yield put(actions.changeRoleStatusFail(error));
	}
}

// Handle Delete Role Status
function* handleDeleteRole(action: any) {
	try {
		const res = yield call(PermissionApi.deleteRole, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Xóa vai trò thành công', 2);
			yield put(actions.deleteRoleSuccess(res));
		} else {
			message.error('Xóa vai trò xảy ra lỗi', 1);
			yield put(actions.deleteRoleFail(res));
		}
	} catch (error) {
		yield put(actions.deleteRoleFail(error));
	}
}

// Handle Get Permissions
function* handleGetPermissions(action: any) {
	try {
		const res = yield call(PermissionApi.getPermissions);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.getPermissionsSuccess(res.data.data));
		} else {
			yield put(actions.getPermissionsFail(res));
		}
	} catch (error) {
		yield put(actions.getPermissionsFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetRoles() {
	yield takeEvery(Keys.GET_ROLES, handleGetRoles);
}
function* watchPostRole() {
	yield takeEvery(Keys.POST_ROLE, handlePostRole);
}
function* watchPutRole() {
	yield takeEvery(Keys.PUT_ROLE, handlePutRole);
}
function* watchDeleteRole() {
	yield takeEvery(Keys.DELETE_ROLE, handleDeleteRole);
}
function* watchChangeRoleStatus() {
	yield takeEvery(Keys.CHANGE_ROLE_STATUS, handleChangeRoleStatus);
}
function* watchGetPermissions() {
	yield takeEvery(Keys.GET_PERMISSIONS, handleGetPermissions);
}
/*-----------------------------------------------------------------*/
const sagas = [
	watchGetRoles,
	watchPostRole,
	watchPutRole,
	watchDeleteRole,
	watchChangeRoleStatus,
	watchGetPermissions,
];
export default sagas;
