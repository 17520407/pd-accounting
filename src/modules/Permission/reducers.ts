import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import {
	IPermissionState,
	initialState,
	PERMISSION_MODAL,
	PERMISSION_CLEAR,
} from './model/IPermissionState';
import { errorRoleMessages } from '../../common/constants';
import { showErrorMessage } from './../../services/showErrorMessage';

export const name = 'PermissionPage';

export const reducer: Reducer<IPermissionState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.HANDLE_ROLE_NAME_CHANGE:
			return onHandleRoleNameChange(state, action);

		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);
		case Keys.HANDLE_ROLE_SELECT:
			return onHandleRoleSelect(state, action);
		case Keys.HANDLE_PERMISSION_SWITCH:
			return onHandlePermissionSwitch(state, action);

		case Keys.GET_PERMISSIONS:
			return onGetPermissions(state, action);
		case Keys.GET_PERMISSIONS_FAIL:
			return onGetPermissionsFail(state, action);
		case Keys.GET_PERMISSIONS_SUCCESS:
			return onGetPermissionsSuccess(state, action);

		case Keys.GET_ROLES:
			return onGetRoles(state, action);
		case Keys.GET_ROLES_SUCCESS:
			return onGetRolesSuccess(state, action);
		case Keys.GET_ROLES_FAIL:
			return onGetRolesFail(state, action);

		case Keys.PUT_ROLE:
			return onPutRole(state, action);
		case Keys.PUT_ROLE_SUCCESS:
			return onPutRoleSuccess(state, action);
		case Keys.PUT_ROLE_FAIL:
			return onPutRoleFail(state, action);

		case Keys.POST_ROLE:
			return onPostRole(state, action);
		case Keys.POST_ROLE_SUCCESS:
			return onPostRoleSuccess(state, action);
		case Keys.POST_ROLE_FAIL:
			return onPostRoleFail(state, action);

		case Keys.DELETE_ROLE:
			return onDeleteRole(state, action);
		case Keys.DELETE_ROLE_SUCCESS:
			return onDeleteRoleSuccess(state, action);
		case Keys.DELETE_ROLE_FAIL:
			return onDeleteRoleFail(state, action);

		case Keys.CHANGE_ROLE_STATUS:
			return onChangeRoleStatus(state, action);
		case Keys.CHANGE_ROLE_STATUS_SUCCESS:
			return onChangeRoleStatusSuccess(state, action);
		case Keys.CHANGE_ROLE_STATUS_FAIL:
			return onChangeRoleStatusFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: IPermissionState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case PERMISSION_MODAL.ADD_EDIT_ROLE:
			return {
				...state,
				toggleModalAddEditRole: !state.toggleModalAddEditRole,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: IPermissionState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case PERMISSION_CLEAR.ROLE_PERMISSIONS_LOADED:
			return {
				...state,
				isPermissionsLoaded: false,
				isRolesLoaded: false,
			};
		case PERMISSION_CLEAR.PERMISSIONS_LOADING:
			return {
				...state,
				isLoadingPermissions: false,
			};
		case PERMISSION_CLEAR.CLEAR_WHEN_UN_MOUNT:
			return {
				...state,
				isLoadingPermissions: false,
				roleSelected: null,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleRoleNameChange = (
	state: IPermissionState,
	action: IActions.IHandleRoleNameChange
) => {
	const roleSelected = state.roleSelected;
	if (action.payload.value !== roleSelected.roleName) {
		return {
			...state,
			roleSelected: {
				...roleSelected,
				roleName: action.payload.value,
			},
			isUpdating: true,
		};
	}
	return {
		...state,
	};
};

const onHandleRoleSelect = (state: IPermissionState, action: IActions.IHandleRoleSelect) => {
	const { rolePermissions } = action.payload;
	const availableRoles: string[] = [];
	const permissionRecords = [...state.permissionRecords];
	permissionRecords.forEach((record) => {
		record['isActive'] = false;
		if (record.children) {
			// tslint:disable-next-line: prefer-for-of
			for (let i = 0; i < record.children.length; i++) {
				record.children[i]['isActive'] = false;
			}
		}
	});
	if (rolePermissions.length > 0) {
		rolePermissions.forEach((permission: { permissionKey: string }) => {
			availableRoles.push(permission.permissionKey);
		});
		permissionRecords.forEach((record) => {
			if (availableRoles.includes(record.key)) {
				record['isActive'] = true;
			} else if (record.children) {
				// tslint:disable-next-line: prefer-for-of
				for (let i = 0; i < record.children.length; i++) {
					if (availableRoles.includes(record.children[i].key)) {
						record.children[i]['isActive'] = true;
					}
				}
			}
		});
	}

	if (state.roleSelected !== null && state.roleSelected !== action.payload) {
		return {
			...state,
			roleSelected: action.payload,
			isLoadingPermissions: true,
			permissionRecords,
		};
	}
	return {
		...state,
		roleSelected: action.payload,
		permissionRecords,
	};
};

const onHandlePermissionSwitch = (
	state: IPermissionState,
	action: IActions.IHandlePermissionSwitch
) => {
	const { rolePermissions } = state.roleSelected;
	const { status, value } = action.payload;
	if (!status) {
		const index = rolePermissions.findIndex(
			(permission: { permissionKey: string }) => permission.permissionKey === value.permissionKey
		);
		rolePermissions.splice(index, 1);
	} else {
		rolePermissions.push(value);
	}

	return {
		...state,
		isUpdating: true,
		roleSelected: {
			...state.roleSelected,
			rolePermissions,
		},
	};
};

const onChangeRoleStatus = (state: IPermissionState, action: IActions.IChangeRoleStatus) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onChangeRoleStatusSuccess = (
	state: IPermissionState,
	action: IActions.IChangeRoleStatusSuccess
) => {
	const { status } = state.roleSelected;
	const roleSelected = {
		...state.roleSelected,
		status: status === 'enable' ? 'disable' : 'enable',
	};
	const roleRecords = [...state.roleRecords];
	const index = roleRecords.findIndex((record) => record._id === roleSelected._id);
	if (index !== -1) {
		roleRecords[index] = roleSelected;
	}

	return {
		...state,
		isProcessing: false,
		roleSelected,
		roleRecords,
	};
};
const onChangeRoleStatusFail = (
	state: IPermissionState,
	action: IActions.IChangeRoleStatusFail
) => {
	showErrorMessage(errorRoleMessages, action.payload[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onGetRoles = (state: IPermissionState, action: IActions.IGetRoles) => {
	return {
		...state,
		isLoadingRoles: true,
	};
};
const onGetRolesSuccess = (state: IPermissionState, action: IActions.IGetRolesSuccess) => {
	if (action.payload.length < 1) {
		return {
			...state,
			isLoadingRoles: false,
		};
	}
	action.payload.pop();
	return {
		...state,
		roleRecords: action.payload,
		isRolesLoaded: true,
		isLoadingRoles: false,
	};
};
const onGetRolesFail = (state: IPermissionState, action: IActions.IGetRolesFail) => {
	return {
		...state,
		isLoadingRoles: false,
	};
};

const onGetPermissions = (state: IPermissionState, action: IActions.IGetPermissions) => {
	return {
		...state,
		isLoadingPermissions: true,
	};
};
const onGetPermissionsSuccess = (
	state: IPermissionState,
	action: IActions.IGetPermissionsSuccess
) => {
	const permissionRecords = action.payload;
	permissionRecords.forEach((record: { [x: string]: any; groupName: any; permissions: any }) => {
		record['title'] = record.groupName;
		record['permissions'].forEach((childRecord: { [x: string]: any }) => {
			childRecord['title'] = childRecord['name'];
			delete childRecord['name'];
		});
		record['isActive'] = false;
		record['children'] = record.permissions;

		delete record['groupName'];
		delete record['groupKey'];
		delete record['permissions'];
	});

	return {
		...state,
		permissionRecords,
		isLoadingPermissions: false,
		isPermissionsLoaded: true,
	};
};
const onGetPermissionsFail = (state: IPermissionState, action: IActions.IGetPermissionsFail) => {
	return {
		...state,
		isLoadingPermissions: false,
	};
};

const onPostRole = (state: IPermissionState, action: IActions.IPostRole) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPostRoleSuccess = (state: IPermissionState, action: IActions.IPostRoleSuccess) => {
	const roleRecords = [...state.roleRecords];
	roleRecords.push(action.payload);
	return {
		...state,
		roleRecords,
		toggleModalAddEditRole: false,
		isProcessing: false,
	};
};
const onPostRoleFail = (state: IPermissionState, action: IActions.IPostRoleFail) => {
	showErrorMessage(errorRoleMessages, action.payload[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onPutRole = (state: IPermissionState, action: IActions.IPutRole) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPutRoleSuccess = (state: IPermissionState, action: IActions.IPutRoleSuccess) => {
	const roleRecords = [...state.roleRecords];
	const index = roleRecords.findIndex((role) => role._id === action.payload._id);
	if (index !== -1) {
		roleRecords[index] = action.payload;
	}

	return {
		...state,
		roleRecords,
		isProcessing: false,
		isUpdating: false,
	};
};
const onPutRoleFail = (state: IPermissionState, action: IActions.IPutRoleFail) => {
	showErrorMessage(errorRoleMessages, action.payload[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteRole = (state: IPermissionState, action: IActions.IDeleteRole) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteRoleSuccess = (state: IPermissionState, action: IActions.IDeleteRoleSuccess) => {
	const roleRecords = [...state.roleRecords];
	const roleSelected = state.roleSelected;
	const index = roleRecords.findIndex((record) => record._id === roleSelected._id);
	if (index !== -1) {
		roleRecords.splice(index, 1);
		// if (roleRecords.length > 0) {
		// 	roleSelected = roleRecords[0];
		// } else {
		// 	roleSelected;
		// }
	}
	return {
		...state,
		roleSelected,
		roleRecords,
		isProcessing: false,
	};
};
const onDeleteRoleFail = (state: IPermissionState, action: IActions.IDeleteRoleFail) => {
	showErrorMessage(errorRoleMessages, action.payload[0].code);
	return {
		...state,
		isProcessing: false,
	};
};
