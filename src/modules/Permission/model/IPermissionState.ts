import { IPermissionObj, IRole } from '../../../common/interfaces';

export enum PERMISSION_MODAL {
	ADD_EDIT_ROLE = 1,
}

export enum PERMISSION_CLEAR {
	ROLE_PERMISSIONS_LOADED = 1,
	PERMISSIONS_LOADING = 2,
	CLEAR_WHEN_UN_MOUNT = 3,
}

export interface IPermissionState {
	roleRecords: any;
	roleSelected: IRole | any;
	permissionRecords: IPermissionObj[];
	toggleModalAddEditRole: boolean;
	isUpdating: boolean;
	isProcessing: boolean;
	isLoadingPermissions: boolean;
	isLoadingRoles: boolean;
	isPermissionsLoaded: boolean;
	isRolesLoaded: boolean;
}

// InitialState
export const initialState: IPermissionState = {
	roleRecords: [],
	roleSelected: null,
	permissionRecords: [],
	toggleModalAddEditRole: false,
	isUpdating: false,
	isPermissionsLoaded: false,
	isRolesLoaded: false,
	isProcessing: false,
	isLoadingRoles: false,
	isLoadingPermissions: false,
};
