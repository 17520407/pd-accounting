/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IHandleRoleSelect
	| IActions.IHandleRoleNameChange
	| IActions.IHandlePermissionSwitch
	| IActions.IGetPermissions
	| IActions.IGetPermissionsSuccess
	| IActions.IGetPermissionsFail
	| IActions.IGetRoles
	| IActions.IGetRolesSuccess
	| IActions.IGetRolesFail
	| IActions.IPostRole
	| IActions.IPostRoleSuccess
	| IActions.IPostRoleFail
	| IActions.IPutRole
	| IActions.IPutRoleSuccess
	| IActions.IPutRoleFail
	| IActions.IDeleteRole
	| IActions.IDeleteRoleSuccess
	| IActions.IDeleteRoleFail
	| IActions.IChangeRoleStatus
	| IActions.IChangeRoleStatusSuccess
	| IActions.IChangeRoleStatusFail;

export default ActionTypes;
