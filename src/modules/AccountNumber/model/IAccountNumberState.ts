import { IPagination } from '../../../common/interfaces';

export enum ACCOUNT_NUMBER_MODAL {
	CREATE_EDIT_ACCOUNT_NUMBER = 1,
}

export enum ACCOUNT_NUMBER_CLEAR {
	ACCOUNT_NUMBER_UPDATE = 1,
}

export interface IAccountNumber {
	key: string | React.ReactText;
	accountName?: string;
	accountNumber?: number;
	parentNumber?: number;
	_id?: string;
	level?: number;
	children?: IAccountNumber[];
}

export interface IAccountNumberRecord {
	key: number;
	accountNumbers: IAccountNumber[];
	circulars: string;
	_id?: string;
}

export interface IAccountNumberState {
	toggleModalCreateEditAccountNumber: boolean;
	isUpdatingAccountNumber: boolean;
	accountNumberRecords: IAccountNumberRecord[];
	pagination: IPagination;
	currentAccountNumber?: IAccountNumberRecord;
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: IAccountNumberState = {
	toggleModalCreateEditAccountNumber: false,
	isUpdatingAccountNumber: false,
	accountNumberRecords: [],
	pagination: {
		totalPage: 0,
		totalRecord: 0,
	},
	isLoading: false,
	isProcessing: false,
};
