import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import {
	ACCOUNT_NUMBER_MODAL,
	ACCOUNT_NUMBER_CLEAR,
	IAccountNumber,
	IAccountNumberRecord,
} from './model/IAccountNumberState';

export const toggleModal = (data: { type: ACCOUNT_NUMBER_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			...data,
		},
	};
};

export const handleCurrentAccountNumber = (data: {
	type: 'update' | 'delete' | 'detail';
	accountNumberRecord: IAccountNumberRecord;
}): IActions.IHandleCurrentAccountNumber => {
	return {
		type: Keys.HANDLE_CURRENT_ACCOUNT_NUMBER,
		payload: {
			...data,
		},
	};
};

export const handleClear = (type: ACCOUNT_NUMBER_CLEAR): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type,
		},
	};
};

//#region Fetch Account Number Actions
export const fetchAccountNumber = (data: {
	page: number;
	limit: number;
}): IActions.IFetchAccountNumber => {
	return {
		type: Keys.FETCH_ACCOUNT_NUMBER,
		payload: data,
	};
};

export const fetchAccountNumberSuccess = (
	res: IAccountNumberRecord[]
): IActions.IFetchAccountNumberSuccess => {
	return {
		type: Keys.FETCH_ACCOUNT_NUMBER_SUCCESS,
		payload: res,
	};
};

export const fetchAccountNumberFail = (res: IError[]): IActions.IFetchAccountNumberFail => {
	return {
		type: Keys.FETCH_ACCOUNT_NUMBER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Create Account Number Actions
export const createAccountNumber = (data: {
	circulars: string;
	accountNumbers: IAccountNumber[];
}): IActions.ICreateAccountNumber => {
	return {
		type: Keys.CREATE_ACCOUNT_NUMBER,
		payload: data,
	};
};

export const createAccountNumberSuccess = (res: any): IActions.ICreateAccountNumberSuccess => {
	return {
		type: Keys.CREATE_ACCOUNT_NUMBER_SUCCESS,
		payload: res,
	};
};

export const createAccountNumberFail = (res: IError[]): IActions.ICreateAccountNumberFail => {
	return {
		type: Keys.CREATE_ACCOUNT_NUMBER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Update Account Number Actions
export const updateAccountNumber = (data: {
	_id: string;
	data: {
		circulars?: string;
		accountNumbers?: IAccountNumber[];
	};
}): IActions.IUpdateAccountNumber => {
	return {
		type: Keys.UPDATE_ACCOUNT_NUMBER,
		payload: data,
	};
};

export const updateAccountNumberSuccess = (res: any): IActions.IUpdateAccountNumberSuccess => {
	return {
		type: Keys.UPDATE_ACCOUNT_NUMBER_SUCCESS,
		payload: res,
	};
};

export const updateAccountNumberFail = (res: IError[]): IActions.IUpdateAccountNumberFail => {
	return {
		type: Keys.UPDATE_ACCOUNT_NUMBER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Delete Account Number Actions
export const deleteAccountNumber = (data: { _id: string }): IActions.IDeleteAccountNumber => {
	return {
		type: Keys.DELETE_ACCOUNT_NUMBER,
		payload: data,
	};
};

export const deleteAccountNumberSuccess = (res: any): IActions.IDeleteAccountNumberSuccess => {
	return {
		type: Keys.DELETE_ACCOUNT_NUMBER_SUCCESS,
		payload: res,
	};
};

export const deleteAccountNumberFail = (res: IError[]): IActions.IDeleteAccountNumberFail => {
	return {
		type: Keys.DELETE_ACCOUNT_NUMBER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
