/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IError } from '../../common/interfaces';
import {
	ACCOUNT_NUMBER_CLEAR,
	ACCOUNT_NUMBER_MODAL,
	IAccountNumber,
	IAccountNumberRecord,
} from './model/IAccountNumberState';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: ACCOUNT_NUMBER_MODAL;
	};
}

export interface IHandleCurrentAccountNumber extends Action {
	readonly type: Keys.HANDLE_CURRENT_ACCOUNT_NUMBER;
	payload: {
		type: 'update' | 'delete' | 'detail';
		accountNumberRecord: IAccountNumberRecord;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: ACCOUNT_NUMBER_CLEAR;
	};
}

//#region Fetch Account Number IActions
export interface IFetchAccountNumber extends Action {
	readonly type: Keys.FETCH_ACCOUNT_NUMBER;
	payload: {
		page: number;
		limit: number;
	};
}

export interface IFetchAccountNumberSuccess extends Action {
	readonly type: Keys.FETCH_ACCOUNT_NUMBER_SUCCESS;
	payload: IAccountNumberRecord[];
}

export interface IFetchAccountNumberFail extends Action {
	readonly type: Keys.FETCH_ACCOUNT_NUMBER_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Create Account Number IActions
export interface ICreateAccountNumber extends Action {
	readonly type: Keys.CREATE_ACCOUNT_NUMBER;
	payload: {
		circulars: string;
		accountNumbers: IAccountNumber[];
	};
}

export interface ICreateAccountNumberSuccess extends Action {
	readonly type: Keys.CREATE_ACCOUNT_NUMBER_SUCCESS;
	payload: any;
}

export interface ICreateAccountNumberFail extends Action {
	readonly type: Keys.CREATE_ACCOUNT_NUMBER_FAIL;
	payload: {
		errors: IError[];
	};
}
//#endregion

//#region Update Account Number IActions
export interface IUpdateAccountNumber extends Action {
	readonly type: Keys.UPDATE_ACCOUNT_NUMBER;
	payload: {
		_id: string;
		data: {
			circulars?: string;
			accountNumbers?: IAccountNumber[];
		};
	};
}

export interface IUpdateAccountNumberSuccess extends Action {
	readonly type: Keys.UPDATE_ACCOUNT_NUMBER_SUCCESS;
	payload: any;
}

export interface IUpdateAccountNumberFail extends Action {
	readonly type: Keys.UPDATE_ACCOUNT_NUMBER_FAIL;
	payload: {
		errors: IError[];
	};
}
//#endregion

//#region Delete Account Number IActions
export interface IDeleteAccountNumber extends Action {
	readonly type: Keys.DELETE_ACCOUNT_NUMBER;
	payload: { _id: string };
}

export interface IDeleteAccountNumberSuccess extends Action {
	readonly type: Keys.DELETE_ACCOUNT_NUMBER_SUCCESS;
	payload: any;
}

export interface IDeleteAccountNumberFail extends Action {
	readonly type: Keys.DELETE_ACCOUNT_NUMBER_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
