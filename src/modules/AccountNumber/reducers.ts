import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import {
	ACCOUNT_NUMBER_MODAL,
	ACCOUNT_NUMBER_CLEAR,
	IAccountNumberState,
	initialState,
} from './model/IAccountNumberState';
import { showErrorMessage } from '../../services/showErrorMessage';
import { errorRoleMessages } from './../../common/constants';

export const name = 'AccountNumberPage';

export const reducer: Reducer<IAccountNumberState, ActionTypes> = (
	state = initialState,
	action
) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CURRENT_ACCOUNT_NUMBER:
			return onHandleCurrentAccountNumber(state, action);

		case Keys.FETCH_ACCOUNT_NUMBER:
			return onFetchAccountNumber(state, action);
		case Keys.FETCH_ACCOUNT_NUMBER_SUCCESS:
			return onFetchAccountNumberSuccess(state, action);
		case Keys.FETCH_ACCOUNT_NUMBER_FAIL:
			return onFetchAccountNumberFail(state, action);

		case Keys.CREATE_ACCOUNT_NUMBER:
			return onCreateAccountNumber(state, action);
		case Keys.CREATE_ACCOUNT_NUMBER_SUCCESS:
			return onCreateAccountNumberSuccess(state, action);
		case Keys.CREATE_ACCOUNT_NUMBER_FAIL:
			return onCreateAccountNumberFail(state, action);

		case Keys.UPDATE_ACCOUNT_NUMBER:
			return onUpdateAccountNumber(state, action);
		case Keys.UPDATE_ACCOUNT_NUMBER_SUCCESS:
			return onUpdateAccountNumberSuccess(state, action);
		case Keys.UPDATE_ACCOUNT_NUMBER_FAIL:
			return onUpdateAccountNumberFail(state, action);

		case Keys.DELETE_ACCOUNT_NUMBER:
			return onDeleteAccountNumber(state, action);
		case Keys.DELETE_ACCOUNT_NUMBER_SUCCESS:
			return onDeleteAccountNumberSuccess(state, action);
		case Keys.DELETE_ACCOUNT_NUMBER_FAIL:
			return onDeleteAccountNumberFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onToggleModal = (state: IAccountNumberState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case ACCOUNT_NUMBER_MODAL.CREATE_EDIT_ACCOUNT_NUMBER:
			return {
				...state,
				toggleModalCreateEditAccountNumber: !state.toggleModalCreateEditAccountNumber,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: IAccountNumberState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case ACCOUNT_NUMBER_CLEAR.ACCOUNT_NUMBER_UPDATE:
			return {
				...state,
				currentAccountNumber: undefined,
				isUpdatingAccountNumber: false,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleCurrentAccountNumber = (
	state: IAccountNumberState,
	action: IActions.IHandleCurrentAccountNumber
) => {
	const { type, accountNumberRecord } = action.payload;
	switch (type) {
		case 'update':
			return {
				...state,
				toggleModalCreateEditAccountNumber: true,
				isUpdatingAccountNumber: true,
				currentAccountNumber: accountNumberRecord,
			};
		default:
			return {
				...state,
				currentAccountNumber: accountNumberRecord,
			};
	}
};

const onFetchAccountNumber = (state: IAccountNumberState, action: IActions.IFetchAccountNumber) => {
	return {
		...state,
		isLoading: true,
	};
};
const onFetchAccountNumberSuccess = (
	state: IAccountNumberState,
	action: IActions.IFetchAccountNumberSuccess
) => {
	// const { data, pagination } = action.payload;
	action.payload.forEach((item, index) => {
		item['key'] = index + 1;
		item.accountNumbers.forEach((child, index) => {
			child['key'] = `${child._id}`;
			if (!child.parentNumber) {
				const filterAccountNumberChild = item.accountNumbers.filter(
					(record) => record.parentNumber === child.accountNumber
				);
				if (filterAccountNumberChild.length > 0) {
					child['children'] = filterAccountNumberChild;
				}
			}
		});
		item.accountNumbers = item.accountNumbers.filter((record) => record.level === 1);
	});
	return {
		...state,
		isLoading: false,
		accountNumberRecords: action.payload,
	};
};
const onFetchAccountNumberFail = (
	state: IAccountNumberState,
	action: IActions.IFetchAccountNumberFail
) => {
	return {
		...state,
		isLoading: false,
	};
};

const onCreateAccountNumber = (
	state: IAccountNumberState,
	action: IActions.ICreateAccountNumber
) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateAccountNumberSuccess = (
	state: IAccountNumberState,
	action: IActions.ICreateAccountNumberSuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalCreateEditAccountNumber: false,
	};
};
const onCreateAccountNumberFail = (
	state: IAccountNumberState,
	action: IActions.ICreateAccountNumberFail
) => {
	showErrorMessage(errorRoleMessages, action.payload.errors[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onUpdateAccountNumber = (
	state: IAccountNumberState,
	action: IActions.IUpdateAccountNumber
) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUpdateAccountNumberSuccess = (
	state: IAccountNumberState,
	action: IActions.IUpdateAccountNumberSuccess
) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onUpdateAccountNumberFail = (
	state: IAccountNumberState,
	action: IActions.IUpdateAccountNumberFail
) => {
	showErrorMessage(errorRoleMessages, action.payload.errors[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteAccountNumber = (
	state: IAccountNumberState,
	action: IActions.IDeleteAccountNumber
) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteAccountNumberSuccess = (
	state: IAccountNumberState,
	action: IActions.IDeleteAccountNumberSuccess
) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onDeleteAccountNumberFail = (
	state: IAccountNumberState,
	action: IActions.IDeleteAccountNumberFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};
