/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IHandleCurrentAccountNumber
	| IActions.ICreateAccountNumber
	| IActions.ICreateAccountNumberSuccess
	| IActions.ICreateAccountNumberFail
	| IActions.IUpdateAccountNumber
	| IActions.IUpdateAccountNumberSuccess
	| IActions.IUpdateAccountNumberFail
	| IActions.IDeleteAccountNumber
	| IActions.IDeleteAccountNumberSuccess
	| IActions.IDeleteAccountNumberFail
	| IActions.IFetchAccountNumber
	| IActions.IFetchAccountNumberSuccess
	| IActions.IFetchAccountNumberFail;

export default ActionTypes;
