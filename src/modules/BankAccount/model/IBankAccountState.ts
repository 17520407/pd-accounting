export enum BANK_ACCOUNT_MODAL {
	ADD_EDIT_BANK = 1,
}

export enum BANK_ACCOUNT_CLEAR {
	BANK_ACCOUNT_UPDATE = 0,
	UPLOAD_IMAGE = 1,
}

export interface IBankAccount {
	key?: number;
	_id?: string;
	name?: string;
	logo?: string;
}

export interface IBankAccountState {
	bankAccountRecords: IBankAccount[];
	toggleModalAddEditBankAccount: boolean;
	isLoadingBankAccount: boolean;
	isUploadLogoSuccess: boolean;
	isProcessing: boolean;
	isUpdatingBankAccount: boolean;
	curBankAccount?: IBankAccount;
	logoUrl: string;
}
// InitialState
export const initialState: IBankAccountState = {
	bankAccountRecords: [],
	toggleModalAddEditBankAccount: false,
	isUploadLogoSuccess: false,
	isLoadingBankAccount: false,
	isProcessing: false,
	isUpdatingBankAccount: false,
	logoUrl: '',
};
