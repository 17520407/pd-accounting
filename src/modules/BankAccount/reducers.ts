import { errorBankCardMessages } from '../../common/constants';
import { showErrorMessage } from '../../services/showErrorMessage';
import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import {
	IBankAccountState,
	initialState,
	BANK_ACCOUNT_MODAL,
	BANK_ACCOUNT_CLEAR,
} from './model/IBankAccountState';

export const name = 'BankAccountPage';

export const reducer: Reducer<IBankAccountState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);
		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CUR_BANK_ACCOUNT:
			return onHandleCurBankAccount(state, action);

		case Keys.UPLOAD_IMAGE:
			return onUploadImage(state, action);
		case Keys.UPLOAD_IMAGE_SUCCESS:
			return onUploadImageSuccess(state, action);
		case Keys.UPLOAD_IMAGE_FAIL:
			return onUploadImageFail(state, action);

		case Keys.DELETE_BANK_ACCOUNT:
			return onDeleteBankAccount(state, action);
		case Keys.DELETE_BANK_ACCOUNT_SUCCESS:
			return onDeleteBankAccountSuccess(state, action);
		case Keys.DELETE_BANK_ACCOUNT_FAIL:
			return onDeleteBankAccountFail(state, action);

		case Keys.UPDATE_BANK_ACCOUNT:
			return onUpdateBankAccount(state, action);
		case Keys.UPDATE_BANK_ACCOUNT_SUCCESS:
			return onUpdateBankAccountSuccess(state, action);
		case Keys.UPDATE_BANK_ACCOUNT_FAIL:
			return onUpdateBankAccountFail(state, action);

		case Keys.CREATE_BANK_ACCOUNT:
			return onCreateBankAccount(state, action);
		case Keys.CREATE_BANK_ACCOUNT_SUCCESS:
			return onCreateBankAccountSuccess(state, action);
		case Keys.CREATE_BANK_ACCOUNT_FAIL:
			return onCreateBankAccountFail(state, action);

		case Keys.FETCH_BANK_ACCOUNT:
			return onFetchBankAccount(state, action);
		case Keys.FETCH_BANK_ACCOUNT_SUCCESS:
			return onFetchBankAccountSuccess(state, action);
		case Keys.FETCH_BANK_ACCOUNT_FAIL:
			return onFetchBankAccountFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: IBankAccountState, action: IActions.IToggleModal) => {
	const { type } = action.payload;

	switch (type) {
		case BANK_ACCOUNT_MODAL.ADD_EDIT_BANK:
			return {
				...state,
				toggleModalAddEditBankAccount: !state.toggleModalAddEditBankAccount,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: IBankAccountState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case BANK_ACCOUNT_CLEAR.BANK_ACCOUNT_UPDATE:
			return {
				...state,
				curBankAccount: undefined,
				isUpdatingBankAccount: false,
			};
		case BANK_ACCOUNT_CLEAR.UPLOAD_IMAGE:
			return {
				...state,
				isUploadLogoSuccess: false,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleCurBankAccount = (
	state: IBankAccountState,
	action: IActions.IHandleCurBankAccount
) => {
	const { type, bankAccount } = action.payload;
	switch (type) {
		case 'update':
			return {
				...state,
				toggleModalAddEditBankAccount: true,
				isUpdatingBankAccount: true,
				curBankAccount: bankAccount,
			};
		default:
			return {
				...state,
				curBankAccount: bankAccount,
			};
	}
};

const onUploadImage = (state: IBankAccountState, action: IActions.IUploadImage) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUploadImageSuccess = (state: IBankAccountState, action: IActions.IUploadImageSuccess) => {
	return {
		...state,
		isUploadLogoSuccess: true,
		isProcessing: false,
		logoUrl: action.payload.url,
	};
};
const onUploadImageFail = (state: IBankAccountState, action: IActions.IUploadImageFail) => {
	showErrorMessage(errorBankCardMessages, action.payload[0].code);
	return {
		...state,
		isUploadLogoSuccess: false,
		isProcessing: false,
	};
};

const onFetchBankAccount = (state: IBankAccountState, action: IActions.IFetchBankAccount) => {
	return {
		...state,
		isLoadingBankAccount: true,
	};
};
const onFetchBankAccountSuccess = (
	state: IBankAccountState,
	action: IActions.IFetchBankAccountSuccess
) => {
	const bankAccountRecords = [...action.payload];
	bankAccountRecords.forEach((record, index) => {
		record['key'] = index + 1;
	});

	return {
		...state,
		bankAccountRecords,
		isLoadingBankAccount: false,
	};
};
const onFetchBankAccountFail = (
	state: IBankAccountState,
	action: IActions.IFetchBankAccountFail
) => {
	return {
		...state,
		isLoadingBankAccount: false,
	};
};

const onCreateBankAccount = (state: IBankAccountState, action: IActions.ICreateBankAccount) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateBankAccountSuccess = (
	state: IBankAccountState,
	action: IActions.ICreateBankAccountSuccess
) => {
	const bankAccountRecords = [...state.bankAccountRecords];
	bankAccountRecords.push(action.payload);
	bankAccountRecords.forEach((item, index) => {
		item['key'] = index + 1;
	});

	return {
		...state,
		toggleModalAddEditBankAccount: false,
		bankAccountRecords,
		isProcessing: false,
	};
};
const onCreateBankAccountFail = (
	state: IBankAccountState,
	action: IActions.ICreateBankAccountFail
) => {
	showErrorMessage(errorBankCardMessages, action.payload[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onUpdateBankAccount = (state: IBankAccountState, action: IActions.IUpdateBankAccount) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUpdateBankAccountSuccess = (
	state: IBankAccountState,
	action: IActions.IUpdateBankAccountSuccess
) => {
	const { _id } = action.payload;
	const bankAccountRecords = [...state.bankAccountRecords];
	const index = bankAccountRecords.findIndex((record) => record._id === _id);
	bankAccountRecords[index] = action.payload;
	return {
		...state,
		bankAccountRecords,
		isProcessing: false,
		isUpdatingBankAccount: false,
		curBankAccount: undefined,
	};
};
const onUpdateBankAccountFail = (
	state: IBankAccountState,
	action: IActions.IUpdateBankAccountFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteBankAccount = (state: IBankAccountState, action: IActions.IDeleteBankAccount) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteBankAccountSuccess = (
	state: IBankAccountState,
	action: IActions.IDeleteBankAccountSuccess
) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onDeleteBankAccountFail = (
	state: IBankAccountState,
	action: IActions.IDeleteBankAccountFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};
