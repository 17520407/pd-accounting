/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IBankAccount, BANK_ACCOUNT_MODAL, BANK_ACCOUNT_CLEAR } from './model/IBankAccountState';
import { IError } from '../../common/interfaces';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: BANK_ACCOUNT_MODAL;
	};
}
export interface IHandleCurBankAccount extends Action {
	readonly type: Keys.HANDLE_CUR_BANK_ACCOUNT;
	payload: {
		type: 'update' | 'delete';
		bankAccount: IBankAccount;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: BANK_ACCOUNT_CLEAR;
	};
}

//#region UPLOAD Image IActions
export interface IUploadImage extends Action {
	readonly type: Keys.UPLOAD_IMAGE;
	payload: File;
}
export interface IUploadImageSuccess extends Action {
	readonly type: Keys.UPLOAD_IMAGE_SUCCESS;
	payload: {
		url: string;
	};
}
export interface IUploadImageFail extends Action {
	readonly type: Keys.UPLOAD_IMAGE_FAIL;
	payload: IError[];
}
//#endregion

//#region Fetch Bank Account IActions
export interface IFetchBankAccount extends Action {
	readonly type: Keys.FETCH_BANK_ACCOUNT;
}
export interface IFetchBankAccountSuccess extends Action {
	readonly type: Keys.FETCH_BANK_ACCOUNT_SUCCESS;
	payload: IBankAccount[];
}
export interface IFetchBankAccountFail extends Action {
	readonly type: Keys.FETCH_BANK_ACCOUNT_FAIL;
	payload: IError[];
}
//#endregion

//#region CREATE Bank Account IActions
export interface ICreateBankAccount extends Action {
	readonly type: Keys.CREATE_BANK_ACCOUNT;
	payload: IBankAccount;
}
export interface ICreateBankAccountSuccess extends Action {
	readonly type: Keys.CREATE_BANK_ACCOUNT_SUCCESS;
	payload: IBankAccount;
}
export interface ICreateBankAccountFail extends Action {
	readonly type: Keys.CREATE_BANK_ACCOUNT_FAIL;
	payload: IError[];
}
//#endregion

//#region UPDATE Bank Account IActions
export interface IUpdateBankAccount extends Action {
	readonly type: Keys.UPDATE_BANK_ACCOUNT;
	payload: {
		_id: string;
		bankAccount: IBankAccount;
	};
}
export interface IUpdateBankAccountSuccess extends Action {
	readonly type: Keys.UPDATE_BANK_ACCOUNT_SUCCESS;
	payload: IBankAccount;
}
export interface IUpdateBankAccountFail extends Action {
	readonly type: Keys.UPDATE_BANK_ACCOUNT_FAIL;
	payload: IError[];
}
//#endregion

//#region DELETE Bank Account IActions
export interface IDeleteBankAccount extends Action {
	readonly type: Keys.DELETE_BANK_ACCOUNT;
	payload: { _id: string };
}

export interface IDeleteBankAccountSuccess extends Action {
	readonly type: Keys.DELETE_BANK_ACCOUNT_SUCCESS;
	payload: any;
}

export interface IDeleteBankAccountFail extends Action {
	readonly type: Keys.DELETE_BANK_ACCOUNT_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
