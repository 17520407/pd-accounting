import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IRoleState, initialState } from './model/IRoleState';

export const name = 'RolePage';

export const reducer: Reducer<IRoleState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.ADMIN_LOGIN:
			return onAdminLogin(state, action);
		case Keys.ADMIN_LOGIN_SUCCESS:
			return onAdminLoginSuccess(state, action);
		case Keys.ADMIN_LOGIN_FAIL:
			return onAdminLoginFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action

const onAdminLogin = (state: IRoleState, action: IActions.IAdminLogin) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onAdminLoginSuccess = (state: IRoleState, action: IActions.IAdminLoginSuccess) => {
	const { accessToken, refreshToken, role } = action.payload.data;
	return {
		...state,
		accessToken,
		refreshToken,
		roleName: role,
		isProcessing: false,
	};
};
const onAdminLoginFail = (state: IRoleState, action: IActions.IAdminLoginFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
