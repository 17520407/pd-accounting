import React from 'react';
import { Input, Button, Form } from 'antd';
import { UserOutlined, LockOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { IRoleProps } from '../../model/IRoleProps';

interface IProps extends IRoleProps {}

interface IInputs {
	username: string;
	password: string;
}

export const LoginForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.LoginPage;

	const onFinish = (data: IInputs) => {
		props.actions.adminLogin(data);
	};
	return (
		<Form form={formInstance} layout="vertical" onFinish={onFinish}>
			<Form.Item
				label="Tên đăng nhập"
				name="username"
				hasFeedback={true}
				rules={[
					{
						required: true,
					},
					{
						max: 30,
					},
				]}
				children={
					<Input
						prefix={<UserOutlined />}
						placeholder="Nhập tên tài khoản"
						allowClear={true}
						disabled={isProcessing}
					/>
				}
			/>
			<Form.Item
				label="Mật khẩu"
				name="password"
				dependencies={['password']}
				hasFeedback={true}
				rules={[
					{
						required: true,
					},
					{
						max: 30,
					},
				]}
				children={
					<Input.Password
						placeholder="Nhập mật khẩu"
						prefix={<LockOutlined />}
						iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
						disabled={isProcessing}
					/>
				}
			/>
			<Form.Item
				children={
					<Button
						block={true}
						danger={true}
						className="text-capitalize"
						type="primary"
						htmlType="submit"
						loading={isProcessing}
					>
						Đăng nhập
					</Button>
				}
			/>
		</Form>
	);
};
