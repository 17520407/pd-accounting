import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IRoleProps {
	store: IStore;
	actions: typeof Actions;
}
