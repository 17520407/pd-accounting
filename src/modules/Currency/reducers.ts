import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import {
	CURRENCY_TYPE_CLEAR,
	CURRENCY_TYPE_MODAL,
	ICurrencyState,
	initialState,
} from './model/ICurrencyState';

export const name = 'CurrencyPage';

export const reducer: Reducer<ICurrencyState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CURRENT_CURRENCY_TYPE:
			return onHandleCurrentCurrencyType(state, action);

		case Keys.FETCH_CURRENCY:
			return onFetchCurrency(state, action);
		case Keys.FETCH_CURRENCY_SUCCESS:
			return onFetchCurrencySuccess(state, action);
		case Keys.FETCH_CURRENCY_FAIL:
			return onFetchCurrencyFail(state, action);

		case Keys.CREATE_CURRENCY:
			return onCreateCurrency(state, action);
		case Keys.CREATE_CURRENCY_SUCCESS:
			return onCreateCurrencySuccess(state, action);
		case Keys.CREATE_CURRENCY_FAIL:
			return onCreateCurrencyFail(state, action);

		case Keys.DELETE_CURRENCY:
			return onDeleteCurrency(state, action);
		case Keys.DELETE_CURRENCY_SUCCESS:
			return onDeleteCurrencySuccess(state, action);
		case Keys.DELETE_CURRENCY_FAIL:
			return onDeleteCurrencyFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onToggleModal = (state: ICurrencyState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case CURRENCY_TYPE_MODAL.CREATE_EDIT_CURRENCY_TYPE:
			return {
				...state,
				toggleModalCreateEditCurrency: !state.toggleModalCreateEditCurrency,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: ICurrencyState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case CURRENCY_TYPE_CLEAR.CURRENCY_TYPE_UPDATE:
			return {
				...state,
				currentCurrencyType: undefined,
				isUpdatingCurrency: false,
			};
		default:
			return {
				...state,
			};
	}
};
const onHandleCurrentCurrencyType = (
	state: ICurrencyState,
	action: IActions.IHandleCurrentCurrencyType
) => {
	const { type, currencyType } = action.payload;
	switch (type) {
		case 'update':
			return {
				...state,
				toggleModalCreateEditCurrency: true,
				isUpdatingCurrency: true,
				currentCurrencyType: currencyType,
			};
		default:
			return {
				...state,
				currentCurrencyType: currencyType,
			};
	}
};

const onFetchCurrency = (state: ICurrencyState, action: IActions.IFetchCurrency) => {
	return {
		...state,
		isLoading: true,
	};
};
const onFetchCurrencySuccess = (state: ICurrencyState, action: IActions.IFetchCurrencySuccess) => {
	const { data, pagination } = action.payload;
	return {
		...state,
		isLoading: false,
		currencyRecords: data,
		pagination,
	};
};
const onFetchCurrencyFail = (state: ICurrencyState, action: IActions.IFetchCurrencyFail) => {
	return {
		...state,
		isLoading: false,
	};
};

const onCreateCurrency = (state: ICurrencyState, action: IActions.ICreateCurrency) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateCurrencySuccess = (
	state: ICurrencyState,
	action: IActions.ICreateCurrencySuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalCreateEditCurrency: false,
	};
};
const onCreateCurrencyFail = (state: ICurrencyState, action: IActions.ICreateCurrencyFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteCurrency = (state: ICurrencyState, action: IActions.IDeleteCurrency) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteCurrencySuccess = (
	state: ICurrencyState,
	action: IActions.IDeleteCurrencySuccess
) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onDeleteCurrencyFail = (state: ICurrencyState, action: IActions.IDeleteCurrencyFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
