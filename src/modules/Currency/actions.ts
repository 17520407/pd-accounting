import { IError, IPagination } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { CURRENCY_TYPE_MODAL, CURRENCY_TYPE_CLEAR, ICurrency } from './model/ICurrencyState';

export const toggleModal = (data: { type: CURRENCY_TYPE_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			...data,
		},
	};
};

export const handleCurrentCurrencyType = (data: {
	type: 'update' | 'delete' | 'detail';
	currencyType: ICurrency;
}): IActions.IHandleCurrentCurrencyType => {
	return {
		type: Keys.HANDLE_CURRENT_CURRENCY_TYPE,
		payload: {
			...data,
		},
	};
};

export const handleClear = (type: CURRENCY_TYPE_CLEAR): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type,
		},
	};
};

//#region Fetch Currency Actions
export const fetchCurrency = (data: { page: number; limit: number }): IActions.IFetchCurrency => {
	return {
		type: Keys.FETCH_CURRENCY,
		payload: data,
	};
};

export const fetchCurrencySuccess = (res: {
	data: any[];
	pagination: IPagination;
}): IActions.IFetchCurrencySuccess => {
	return {
		type: Keys.FETCH_CURRENCY_SUCCESS,
		payload: res,
	};
};

export const fetchCurrencyFail = (res: IError[]): IActions.IFetchCurrencyFail => {
	return {
		type: Keys.FETCH_CURRENCY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Create Currency Actions
export const createCurrency = (data: { name: string }): IActions.ICreateCurrency => {
	return {
		type: Keys.CREATE_CURRENCY,
		payload: data,
	};
};

export const createCurrencySuccess = (res: any): IActions.ICreateCurrencySuccess => {
	return {
		type: Keys.CREATE_CURRENCY_SUCCESS,
		payload: res,
	};
};

export const createCurrencyFail = (res: IError[]): IActions.ICreateCurrencyFail => {
	return {
		type: Keys.CREATE_CURRENCY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Update Currency Actions
export const updateCurrency = (data: {
	_id: string;
	currency: ICurrency;
}): IActions.IUpdateCurrency => {
	return {
		type: Keys.UPDATE_CURRENCY,
		payload: data,
	};
};

export const updateCurrencySuccess = (res: any): IActions.IUpdateCurrencySuccess => {
	return {
		type: Keys.UPDATE_CURRENCY_SUCCESS,
		payload: res,
	};
};

export const updateCurrencyFail = (res: IError[]): IActions.IUpdateCurrencyFail => {
	return {
		type: Keys.UPDATE_CURRENCY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Delete Currency Actions
export const deleteCurrency = (data: { _id: string }): IActions.IDeleteCurrency => {
	return {
		type: Keys.DELETE_CURRENCY,
		payload: data,
	};
};

export const deleteCurrencySuccess = (res: any): IActions.IDeleteCurrencySuccess => {
	return {
		type: Keys.DELETE_CURRENCY_SUCCESS,
		payload: res,
	};
};

export const deleteCurrencyFail = (res: IError[]): IActions.IDeleteCurrencyFail => {
	return {
		type: Keys.DELETE_CURRENCY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
