/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IHandleCurrentCurrencyType
	| IActions.ICreateCurrency
	| IActions.ICreateCurrencySuccess
	| IActions.ICreateCurrencyFail
	| IActions.IUpdateCurrency
	| IActions.IUpdateCurrencySuccess
	| IActions.IUpdateCurrencyFail
	| IActions.IDeleteCurrency
	| IActions.IDeleteCurrencySuccess
	| IActions.IDeleteCurrencyFail
	| IActions.IFetchCurrency
	| IActions.IFetchCurrencySuccess
	| IActions.IFetchCurrencyFail;

export default ActionTypes;
