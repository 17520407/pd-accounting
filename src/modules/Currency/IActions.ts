import { IPagination } from './../../common/interfaces';
/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IError } from '../../common/interfaces';
import { CURRENCY_TYPE_MODAL, CURRENCY_TYPE_CLEAR, ICurrency } from './model/ICurrencyState';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: CURRENCY_TYPE_MODAL;
	};
}

export interface IHandleCurrentCurrencyType extends Action {
	readonly type: Keys.HANDLE_CURRENT_CURRENCY_TYPE;
	payload: {
		type: 'update' | 'delete' | 'detail';
		currencyType: ICurrency;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: CURRENCY_TYPE_CLEAR;
	};
}

//#region Fetch Currency IActions
export interface IFetchCurrency extends Action {
	readonly type: Keys.FETCH_CURRENCY;
	payload: {
		page: number;
		limit: number;
	};
}

export interface IFetchCurrencySuccess extends Action {
	readonly type: Keys.FETCH_CURRENCY_SUCCESS;
	payload: {
		data: any[];
		pagination: IPagination;
	};
}

export interface IFetchCurrencyFail extends Action {
	readonly type: Keys.FETCH_CURRENCY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Create Currency IActions
export interface ICreateCurrency extends Action {
	readonly type: Keys.CREATE_CURRENCY;
	payload: { name: string };
}

export interface ICreateCurrencySuccess extends Action {
	readonly type: Keys.CREATE_CURRENCY_SUCCESS;
	payload: any;
}

export interface ICreateCurrencyFail extends Action {
	readonly type: Keys.CREATE_CURRENCY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Update Currency IActions
export interface IUpdateCurrency extends Action {
	readonly type: Keys.UPDATE_CURRENCY;
	payload: { _id: string; currency: ICurrency };
}

export interface IUpdateCurrencySuccess extends Action {
	readonly type: Keys.UPDATE_CURRENCY_SUCCESS;
	payload: any;
}

export interface IUpdateCurrencyFail extends Action {
	readonly type: Keys.UPDATE_CURRENCY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Delete Currency IActions
export interface IDeleteCurrency extends Action {
	readonly type: Keys.DELETE_CURRENCY;
	payload: { _id: string };
}

export interface IDeleteCurrencySuccess extends Action {
	readonly type: Keys.DELETE_CURRENCY_SUCCESS;
	payload: any;
}

export interface IDeleteCurrencyFail extends Action {
	readonly type: Keys.DELETE_CURRENCY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
