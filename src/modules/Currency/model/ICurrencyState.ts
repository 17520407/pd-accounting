import { IPagination } from './../../../common/interfaces';
export enum CURRENCY_TYPE_MODAL {
	CREATE_EDIT_CURRENCY_TYPE = 1,
}

export enum CURRENCY_TYPE_CLEAR {
	CURRENCY_TYPE_UPDATE = 1,
}

export interface ICurrency {
	_id?: string;
	name: string;
	defaultExchangeRate: string;
}

export interface ICurrencyState {
	toggleModalCreateEditCurrency: boolean;
	isUpdatingCurrency: boolean;
	currencyRecords: ICurrency[];
	pagination: IPagination;
	currentCurrencyType?: ICurrency;
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: ICurrencyState = {
	toggleModalCreateEditCurrency: false,
	isUpdatingCurrency: false,
	currencyRecords: [],
	pagination: {
		totalPage: 0,
		totalRecord: 0,
	},
	isLoading: false,
	isProcessing: false,
};
