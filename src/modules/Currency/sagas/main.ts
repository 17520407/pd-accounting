import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as CurrencyApi from '../../../api/currency';
import { message } from 'antd';

// Handle Fetch Currency
function* handleFetchCurrency(action: any) {
	try {
		const res = yield call(CurrencyApi.fetchCurrencyType, action.payload);
		if (res.status === 200) {
			yield delay(200);
			yield put(
				actions.fetchCurrencySuccess({
					data: res.data.data,
					pagination: res.data.data.pop(),
				})
			);
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchCurrencyFail(error));
	}
}

// Handle Create Currency
function* handleCreateCurrency(action: any) {
	try {
		const res = yield call(CurrencyApi.createCurrencyType, action.payload);
		if (res.status === 200) {
			message.success('Tạo mới loại tiền tệ thành công', 2);
			yield put(actions.createCurrencySuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.createCurrencyFail(error));
	}
}

// Handle Update Currency
function* handleUpdateCurrency(action: any) {
	try {
		const res = yield call(CurrencyApi.updateCurrencyType, action.payload);
		if (res.status === 200) {
			message.success('Cập nhật loại công ty thành công', 2);
			yield put(actions.updateCurrencySuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.updateCurrencyFail(error));
	}
}

// Handle Delete Currency
function* handleDeleteCurrency(action: any) {
	try {
		const res = yield call(CurrencyApi.deleteCurrencyType, action.payload);
		if (res.status === 200) {
			message.success('Xóa loại công ty thành công', 2);
			yield put(actions.deleteCurrencySuccess(res.data.data));
			yield put(actions.fetchCurrency({ page: 0, limit: 10 }));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.deleteCurrencyFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchFetchCurrency() {
	yield takeEvery(Keys.FETCH_CURRENCY, handleFetchCurrency);
}
function* watchCreateCurrency() {
	yield takeEvery(Keys.CREATE_CURRENCY, handleCreateCurrency);
}
function* watchUpdateCurrency() {
	yield takeEvery(Keys.UPDATE_CURRENCY, handleUpdateCurrency);
}
function* watchDeleteCurrency() {
	yield takeEvery(Keys.DELETE_CURRENCY, handleDeleteCurrency);
}
/*-----------------------------------------------------------------*/
const sagas = [watchFetchCurrency, watchCreateCurrency, watchUpdateCurrency, watchDeleteCurrency];
export default sagas;
