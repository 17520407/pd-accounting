import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Divider, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { ICurrencyProps } from '../model/ICurrencyProps';
import { CustomBreadCrumb } from '../../../components';
import { CurrencyTypeTable } from './Table';
import { CreateOrEditCurrencyTypeForm } from './Form';
import { CURRENCY_TYPE_MODAL } from '../model/ICurrencyState';

interface IProps extends RouteComponentProps, ICurrencyProps {}

export const CurrencyPage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.fetchCurrency({
			page: 0,
			limit: 10,
		});
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb title="Danh sách loại tệ" path="/management/currency" routeName="Quản lý" />
			<div className="box">
				<div className="box_content">
					<Button
						type="primary"
						icon={<PlusOutlined />}
						onClick={() =>
							props.actions.toggleModal({
								type: CURRENCY_TYPE_MODAL.CREATE_EDIT_CURRENCY_TYPE,
							})
						}
					>
						Tạo mới loại tệ
					</Button>
					<Divider />
					<CurrencyTypeTable {...props} />
				</div>
			</div>
			<CreateOrEditCurrencyTypeForm {...props} />
		</React.Fragment>
	);
};
