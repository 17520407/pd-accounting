import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { ICurrencyProps } from '../../model/ICurrencyProps';
import { IColumn } from '../../../../common/interfaces';
import { useMediaQuery } from 'react-responsive';
import { ICurrency } from '../../model/ICurrencyState';

const columns = (props: ICurrencyProps): IColumn[] => {
	return [
		{
			title: 'Loại tiền tệ',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Tỉ giá',
			dataIndex: 'defaultExchangeRate',
			key: 'defaultExchangeRate',
		},
		{
			title: 'Hành động',
			dataIndex: 'operation',
			render: (text: any, record: ICurrency) => (
				<Space>
					<Tooltip placement="top" title="Chỉnh sửa">
						<Button
							type="text"
							onClick={() => {
								props.actions.handleCurrentCurrencyType({
									type: 'update',
									currencyType: record,
								});
							}}
							icon={<EditOutlined className="text-primary" />}
						/>
					</Tooltip>
					<Tooltip placement="top" title="Xóa">
						<Popconfirm
							title="Bạn có chắc chắn xoá loại tiền tệ này?"
							onConfirm={() => {
								props.actions.handleCurrentCurrencyType({
									type: 'delete',
									currencyType: record,
								});
								props.actions.deleteCurrency({ _id: record?._id as string });
							}}
							cancelText="Huỷ"
							okText="Xoá"
						>
							<DeleteOutlined className="text-danger" />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

interface IProps extends ICurrencyProps {}

export const CurrencyTypeTable: React.FC<IProps> = (props) => {
	const isTabletPortrait = useMediaQuery({ query: '(max-width: 767px)' });
	const { currencyRecords, pagination, isLoading, isProcessing } = props.store.CurrencyPage;

	return (
		<Table
			locale={{
				emptyText: <Empty description="Không có dữ liệu" />,
			}}
			rowKey="_id"
			scroll={{
				x: isTabletPortrait ? 1000 : 0,
			}}
			columns={columns(props)}
			loading={isLoading || isProcessing}
			dataSource={currencyRecords}
			pagination={{ total: pagination.totalRecord }}
		/>
	);
};
