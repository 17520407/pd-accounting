import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import {
	ICompanyTypeState,
	COMPANY_TYPE_MODAL,
	initialState,
	COMPANY_TYPE_CLEAR,
} from './model/ICompanyTypeState';

export const name = 'CompanyTypePage';

export const reducer: Reducer<ICompanyTypeState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CURRENT_COMPANY_TYPE:
			return onHandleCurrentCompanyType(state, action);

		case Keys.FETCH_COMPANY_TYPES:
			return onFetchCompanyTypes(state, action);
		case Keys.FETCH_COMPANY_TYPES_SUCCESS:
			return onFetchCompanyTypesSuccess(state, action);
		case Keys.FETCH_COMPANY_TYPES_FAIL:
			return onFetchCompanyTypesFail(state, action);

		case Keys.CREATE_COMPANY_TYPE:
			return onCreateCompanyType(state, action);
		case Keys.CREATE_COMPANY_TYPE_SUCCESS:
			return onCreateCompanyTypeSuccess(state, action);
		case Keys.CREATE_COMPANY_TYPE_FAIL:
			return onCreateCompanyTypeFail(state, action);

		case Keys.DELETE_COMPANY_TYPE:
			return onDeleteCompanyType(state, action);
		case Keys.DELETE_COMPANY_TYPE_SUCCESS:
			return onDeleteCompanyTypeSuccess(state, action);
		case Keys.DELETE_COMPANY_TYPE_FAIL:
			return onDeleteCompanyTypeFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action

const onToggleModal = (state: ICompanyTypeState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case COMPANY_TYPE_MODAL.CREATE_EDIT_COMPANY_TYPE:
			return {
				...state,
				toggleModalCreateEditCompanyType: !state.toggleModalCreateEditCompanyType,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: ICompanyTypeState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case COMPANY_TYPE_CLEAR.COMPANY_TYPE_UPDATE:
			return {
				...state,
				currentCompanyType: undefined,
				isUpdatingCompanyType: false,
			};
		default:
			return {
				...state,
			};
	}
};
const onHandleCurrentCompanyType = (
	state: ICompanyTypeState,
	action: IActions.IHandleCurrentCompanyType
) => {
	const { type, companyType } = action.payload;
	switch (type) {
		case 'update':
			return {
				...state,
				toggleModalCreateEditCompanyType: true,
				isUpdatingCompanyType: true,
				currentCompanyType: companyType,
			};
		default:
			return {
				...state,
				currentCompanyType: companyType,
			};
	}
};

const onFetchCompanyTypes = (state: ICompanyTypeState, action: IActions.IFetchCompanyTypes) => {
	return {
		...state,
		isLoading: true,
	};
};
const onFetchCompanyTypesSuccess = (
	state: ICompanyTypeState,
	action: IActions.IFetchCompanyTypesSuccess
) => {
	return {
		...state,
		isLoading: false,
		companyTypeRecords: action.payload,
	};
};
const onFetchCompanyTypesFail = (
	state: ICompanyTypeState,
	action: IActions.IFetchCompanyTypesFail
) => {
	return {
		...state,
		isLoading: false,
	};
};

const onCreateCompanyType = (state: ICompanyTypeState, action: IActions.ICreateCompanyType) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateCompanyTypeSuccess = (
	state: ICompanyTypeState,
	action: IActions.ICreateCompanyTypeSuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalCreateEditCompanyType: false,
	};
};
const onCreateCompanyTypeFail = (
	state: ICompanyTypeState,
	action: IActions.ICreateCompanyTypeFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteCompanyType = (state: ICompanyTypeState, action: IActions.IDeleteCompanyType) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteCompanyTypeSuccess = (
	state: ICompanyTypeState,
	action: IActions.IDeleteCompanyTypeSuccess
) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onDeleteCompanyTypeFail = (
	state: ICompanyTypeState,
	action: IActions.IDeleteCompanyTypeFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};
