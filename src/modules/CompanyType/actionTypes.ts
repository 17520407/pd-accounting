/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IHandleCurrentCompanyType
	| IActions.ICreateCompanyType
	| IActions.ICreateCompanyTypeSuccess
	| IActions.ICreateCompanyTypeFail
	| IActions.IUpdateCompanyType
	| IActions.IUpdateCompanyTypeSuccess
	| IActions.IUpdateCompanyTypeFail
	| IActions.IDeleteCompanyType
	| IActions.IDeleteCompanyTypeSuccess
	| IActions.IDeleteCompanyTypeFail
	| IActions.IFetchCompanyTypes
	| IActions.IFetchCompanyTypesSuccess
	| IActions.IFetchCompanyTypesFail;

export default ActionTypes;
