import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ICompanyTypeProps {
	store: IStore;
	actions: typeof Actions;
}
