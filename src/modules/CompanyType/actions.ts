import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { COMPANY_TYPE_MODAL, COMPANY_TYPE_CLEAR, ICompanyType } from './model/ICompanyTypeState';

export const toggleModal = (data: { type: COMPANY_TYPE_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			...data,
		},
	};
};

export const handleCurrentCompanyType = (data: {
	type: 'update' | 'delete' | 'detail';
	companyType: ICompanyType;
}): IActions.IHandleCurrentCompanyType => {
	return {
		type: Keys.HANDLE_CURRENT_COMPANY_TYPE,
		payload: {
			...data,
		},
	};
};

export const handleClear = (type: COMPANY_TYPE_CLEAR): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type,
		},
	};
};

//#region Fetch Company Types Actions
export const fetchCompanyTypes = (data: {
	page: number;
	limit: number;
}): IActions.IFetchCompanyTypes => {
	return {
		type: Keys.FETCH_COMPANY_TYPES,
		payload: data,
	};
};

export const fetchCompanyTypesSuccess = (res: any): IActions.IFetchCompanyTypesSuccess => {
	return {
		type: Keys.FETCH_COMPANY_TYPES_SUCCESS,
		payload: res,
	};
};

export const fetchCompanyTypesFail = (res: IError[]): IActions.IFetchCompanyTypesFail => {
	return {
		type: Keys.FETCH_COMPANY_TYPES_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Create Company Type Actions
export const createCompanyType = (data: { name: string }): IActions.ICreateCompanyType => {
	return {
		type: Keys.CREATE_COMPANY_TYPE,
		payload: data,
	};
};

export const createCompanyTypeSuccess = (res: any): IActions.ICreateCompanyTypeSuccess => {
	return {
		type: Keys.CREATE_COMPANY_TYPE_SUCCESS,
		payload: res,
	};
};

export const createCompanyTypeFail = (res: IError[]): IActions.ICreateCompanyTypeFail => {
	return {
		type: Keys.CREATE_COMPANY_TYPE_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Update Company Type Actions
export const updateCompanyType = (data: {
	_id: string;
	companyType: ICompanyType;
}): IActions.IUpdateCompanyType => {
	return {
		type: Keys.UPDATE_COMPANY_TYPE,
		payload: data,
	};
};

export const updateCompanyTypeSuccess = (res: any): IActions.IUpdateCompanyTypeSuccess => {
	return {
		type: Keys.UPDATE_COMPANY_TYPE_SUCCESS,
		payload: res,
	};
};

export const updateCompanyTypeFail = (res: IError[]): IActions.IUpdateCompanyTypeFail => {
	return {
		type: Keys.UPDATE_COMPANY_TYPE_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Delete Company Type Actions
export const deleteCompanyType = (data: { _id: string }): IActions.IDeleteCompanyType => {
	return {
		type: Keys.DELETE_COMPANY_TYPE,
		payload: data,
	};
};

export const deleteCompanyTypeSuccess = (res: any): IActions.IDeleteCompanyTypeSuccess => {
	return {
		type: Keys.DELETE_COMPANY_TYPE_SUCCESS,
		payload: res,
	};
};

export const deleteCompanyTypeFail = (res: IError[]): IActions.IDeleteCompanyTypeFail => {
	return {
		type: Keys.DELETE_COMPANY_TYPE_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
