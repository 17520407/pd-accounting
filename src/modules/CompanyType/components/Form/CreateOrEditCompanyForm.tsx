import React from 'react';
import { Input, Button, Typography, Modal, Form, Space } from 'antd';
import { ICompanyTypeProps } from '../../model/ICompanyTypeProps';
import { COMPANY_TYPE_CLEAR, COMPANY_TYPE_MODAL } from '../../model/ICompanyTypeState';

const { Title } = Typography;

interface IProps extends ICompanyTypeProps {}

interface IInputs {
	name: string;
}

export const CreateOrEditCompanyTypeForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const {
		isProcessing,
		currentCompanyType,
		isUpdatingCompanyType,
		toggleModalCreateEditCompanyType,
	} = props.store.CompanyTypePage;

	React.useEffect(() => {
		if (currentCompanyType) {
			formInstance.setFieldsValue({
				name: currentCompanyType.name,
			});
		}
	}, [isUpdatingCompanyType, formInstance, currentCompanyType]);

	const onFinish = (data: IInputs) => {
		if (isUpdatingCompanyType) {
			props.actions.updateCompanyType({
				_id: currentCompanyType?._id as string,
				companyType: data,
			});
		} else {
			props.actions.createCompanyType(data);
		}
	};

	return (
		<Modal
			visible={toggleModalCreateEditCompanyType}
			onCancel={() => {
				formInstance.resetFields();
				if (isUpdatingCompanyType) {
					props.actions.handleClear(COMPANY_TYPE_CLEAR.COMPANY_TYPE_UPDATE);
				}
				props.actions.toggleModal({
					type: COMPANY_TYPE_MODAL.CREATE_EDIT_COMPANY_TYPE,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			maskClosable={false}
			footer={null}
		>
			<Title level={4}>Tạo mới loại công ty</Title>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Form.Item
					label="Tên loại công ty"
					name="name"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={<Input allowClear={true} disabled={isProcessing} />}
				/>

				<div className="d-flex w-100 justify-content-end">
					<Space>
						<Button
							className="text-capitalize"
							onClick={() => {
								props.actions.toggleModal({
									type: COMPANY_TYPE_MODAL.CREATE_EDIT_COMPANY_TYPE,
								});
							}}
						>
							Đóng
						</Button>
						<Button
							className="text-capitalize"
							type="primary"
							htmlType="submit"
							loading={isProcessing}
						>
							{isUpdatingCompanyType ? 'Cập nhật' : 'Tạo'}
						</Button>
					</Space>
				</div>
			</Form>
		</Modal>
	);
};
