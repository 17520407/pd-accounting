import { message } from 'antd';
import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as CompanyApi from '../../../api/company';

// Handle Fetch Company Types
function* handleFetchCompanyTypes(action: any) {
	try {
		const res = yield call(CompanyApi.fetchCompanyTypes, action.payload);
		if (res.status === 200) {
			yield delay(200);
			yield put(actions.fetchCompanyTypesSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchCompanyTypesFail(error));
	}
}

// Handle Create Company Type
function* handleCreateCompanyType(action: any) {
	try {
		const res = yield call(CompanyApi.createCompanyType, action.payload);
		if (res.status === 200) {
			message.success('Tạo mới loại công ty thành công', 2);
			yield put(actions.createCompanyTypeSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.createCompanyTypeFail(error));
	}
}

// Handle Update Company Type
function* handleUpdateCompanyType(action: any) {
	try {
		const res = yield call(CompanyApi.updateCompanyType, action.payload);
		if (res.status === 200) {
			message.success('Cập nhật loại công ty thành công', 2);
			yield put(actions.updateCompanyTypeSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.updateCompanyTypeFail(error));
	}
}

// Handle Delete Company Type
function* handleDeleteCompanyType(action: any) {
	try {
		const res = yield call(CompanyApi.deleteCompanyType, action.payload);
		if (res.status === 200) {
			message.success('Xóa loại công ty thành công', 2);
			yield put(actions.deleteCompanyTypeSuccess(res.data.data));
			yield put(actions.fetchCompanyTypes({ page: 0, limit: 10 }));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.deleteCompanyTypeFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchFetchCompanyTypes() {
	yield takeEvery(Keys.FETCH_COMPANY_TYPES, handleFetchCompanyTypes);
}
function* watchCreateCompanyType() {
	yield takeEvery(Keys.CREATE_COMPANY_TYPE, handleCreateCompanyType);
}
function* watchUpdateCompanyType() {
	yield takeEvery(Keys.UPDATE_COMPANY_TYPE, handleUpdateCompanyType);
}
function* watchDeleteCompanyType() {
	yield takeEvery(Keys.DELETE_COMPANY_TYPE, handleDeleteCompanyType);
}
/*-----------------------------------------------------------------*/
const sagas = [
	watchFetchCompanyTypes,
	watchCreateCompanyType,
	watchUpdateCompanyType,
	watchDeleteCompanyType,
];
export default sagas;
