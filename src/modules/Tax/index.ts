/**
 * @file index
 * Please import some config when register new module
 * Some base file neeed to update
 * * src/redux/rootReducers.js
 * * src/redux/rootSagas.js
 */

import sagas from './sagas';
import { reducer, name } from './reducers';
import { ITaxState, initialState } from './model/ITaxState';

export { name, sagas, initialState };
export type { ITaxState };

export default reducer;
