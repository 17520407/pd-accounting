import { errorBankCardMessages } from '../../common/constants';
import { showErrorMessage } from '../../services/showErrorMessage';
import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { ITaxState, initialState, TAX_MODAL, TAX_CLEAR } from './model/ITaxState';

export const name = 'TaxPage';

export const reducer: Reducer<ITaxState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);
		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CUR_TAX:
			return onHandleCurTax(state, action);

		case Keys.UPDATE_TAX:
			return onUpdateTax(state, action);
		case Keys.UPDATE_TAX_SUCCESS:
			return onUpdateTaxSuccess(state, action);
		case Keys.UPDATE_TAX_FAIL:
			return onUpdateTaxFail(state, action);

		case Keys.CREATE_TAX:
			return onCreateTax(state, action);
		case Keys.CREATE_TAX_SUCCESS:
			return onCreateTaxSuccess(state, action);
		case Keys.CREATE_TAX_FAIL:
			return onCreateTaxFail(state, action);

		case Keys.FETCH_TAXES:
			return onFetchTaxes(state, action);
		case Keys.FETCH_TAXES_SUCCESS:
			return onFetchTaxesSuccess(state, action);
		case Keys.FETCH_TAXES_FAIL:
			return onFetchTaxesFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: ITaxState, action: IActions.IToggleModal) => {
	const { type } = action.payload;

	switch (type) {
		case TAX_MODAL.ADD_EDIT_TAX:
			return {
				...state,
				toggleModalAddEditTax: !state.toggleModalAddEditTax,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: ITaxState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case TAX_CLEAR.TAX_UPDATE:
			return {
				...state,
				curTax: undefined,
				isUpdatingTax: false,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleCurTax = (state: ITaxState, action: IActions.IHandleCurTax) => {
	const { type, tax } = action.payload;
	switch (type) {
		case 'update':
			return {
				...state,
				toggleModalAddEditTax: true,
				isUpdatingTax: true,
				curTax: tax,
			};
		default:
			return {
				...state,
				curTax: tax,
			};
	}
};

const onFetchTaxes = (state: ITaxState, action: IActions.IFetchTaxes) => {
	return {
		...state,
		isLoadingTax: true,
	};
};
const onFetchTaxesSuccess = (state: ITaxState, action: IActions.IFetchTaxesSuccess) => {
	const taxRecords = [...action.payload];
	taxRecords.forEach((record, index) => {
		record['key'] = index + 1;
	});

	return {
		...state,
		taxRecords,
		isLoadingTax: false,
	};
};
const onFetchTaxesFail = (state: ITaxState, action: IActions.IFetchTaxesFail) => {
	return {
		...state,
		isLoadingTax: false,
	};
};

const onCreateTax = (state: ITaxState, action: IActions.ICreateTax) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateTaxSuccess = (state: ITaxState, action: IActions.ICreateTaxSuccess) => {
	const taxRecords = [...state.taxRecords];
	taxRecords.push(action.payload);
	return {
		...state,
		isProcessing: false,
		taxRecords,
	};
};
const onCreateTaxFail = (state: ITaxState, action: IActions.ICreateTaxFail) => {
	showErrorMessage(errorBankCardMessages, action.payload[0].code);
	return {
		...state,
		isProcessing: false,
	};
};

const onUpdateTax = (state: ITaxState, action: IActions.IUpdateTax) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUpdateTaxSuccess = (state: ITaxState, action: IActions.IUpdateTaxSuccess) => {
	const taxRecords = [...state.taxRecords];
	const index = taxRecords.findIndex((record) => record._id === action.payload._id);
	if (index > -1) {
		taxRecords[index] = action.payload;
	}
	taxRecords.forEach((record, index) => {
		record['key'] = index + 1;
	});
	return {
		...state,
		toggleModalAddEditTax: false,
		isProcessing: false,
		taxRecords,
		curTax: undefined,
		isUpdatingTax: false,
	};
};
const onUpdateTaxFail = (state: ITaxState, action: IActions.IUpdateTaxFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
