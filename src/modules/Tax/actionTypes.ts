/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IHandleCurTax
	| IActions.IFetchTaxes
	| IActions.IFetchTaxesSuccess
	| IActions.IFetchTaxesFail
	| IActions.ICreateTax
	| IActions.ICreateTaxSuccess
	| IActions.ICreateTaxFail
	| IActions.IUpdateTax
	| IActions.IUpdateTaxSuccess
	| IActions.IUpdateTaxFail;

export default ActionTypes;
