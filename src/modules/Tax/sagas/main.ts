import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { message } from 'antd';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as TaxApi from '../../../api/tax';

// Handle GET Taxes
function* handleFetchTaxes(action: any) {
	try {
		const res = yield call(TaxApi.fetchTaxes, action.payload);
		if (res.status === 200) {
			yield put(actions.fetchTaxesSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchTaxesFail(error));
	}
}

// Handle CREATE Tax
function* handleCreateTax(action: any) {
	try {
		const res = yield call(TaxApi.createTax, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Tạo mới thuế thành công', 0.5);
			yield put(actions.createTaxSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.createTaxFail(error));
	}
}

// Handle UPDATE Tax
function* handleUpdateTax(action: any) {
	try {
		const res = yield call(TaxApi.updateTax, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Cập nhật thuế thành công', 2);
			yield put(actions.updateTaxSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.updateTaxFail(error));
	}
}
/*-----------------------------------------------------------------*/
function* watchFetchTaxes() {
	yield takeEvery(Keys.FETCH_TAXES, handleFetchTaxes);
}
function* watchCreateTax() {
	yield takeEvery(Keys.CREATE_TAX, handleCreateTax);
}
function* watchUpdateTax() {
	yield takeEvery(Keys.UPDATE_TAX, handleUpdateTax);
}
/*-----------------------------------------------------------------*/
const sagas = [watchFetchTaxes, watchCreateTax, watchUpdateTax];
export default sagas;
