import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ITaxProps {
	store: IStore;
	actions: typeof Actions;
}
