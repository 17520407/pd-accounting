/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { ITaxRecord, TAX_MODAL, TAX_CLEAR } from './model/ITaxState';
import { IError } from '../../common/interfaces';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: TAX_MODAL;
	};
}
export interface IHandleCurTax extends Action {
	readonly type: Keys.HANDLE_CUR_TAX;
	payload: {
		type: 'update' | 'delete';
		tax: ITaxRecord;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: TAX_CLEAR;
	};
}

//#region Fetch Taxes IActions
export interface IFetchTaxes extends Action {
	readonly type: Keys.FETCH_TAXES;
}
export interface IFetchTaxesSuccess extends Action {
	readonly type: Keys.FETCH_TAXES_SUCCESS;
	payload: ITaxRecord[];
}
export interface IFetchTaxesFail extends Action {
	readonly type: Keys.FETCH_TAXES_FAIL;
	payload: IError[];
}
//#endregion

//#region CREATE Tax IActions
export interface ICreateTax extends Action {
	readonly type: Keys.CREATE_TAX;
	payload: ITaxRecord;
}
export interface ICreateTaxSuccess extends Action {
	readonly type: Keys.CREATE_TAX_SUCCESS;
	payload: ITaxRecord;
}
export interface ICreateTaxFail extends Action {
	readonly type: Keys.CREATE_TAX_FAIL;
	payload: IError[];
}
//#endregion

//#region UPDATE Tax IActions
export interface IUpdateTax extends Action {
	readonly type: Keys.UPDATE_TAX;
	payload: {
		_id: string;
		tax: ITaxRecord;
	};
}
export interface IUpdateTaxSuccess extends Action {
	readonly type: Keys.UPDATE_TAX_SUCCESS;
	payload: ITaxRecord;
}
export interface IUpdateTaxFail extends Action {
	readonly type: Keys.UPDATE_TAX_FAIL;
	payload: IError[];
}
//#endregion
