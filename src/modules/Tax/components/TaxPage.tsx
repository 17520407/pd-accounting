import * as React from 'react';
import { ITaxProps } from '../model/ITaxProps';
import { TAX_MODAL } from '../model/ITaxState';
import { RouteComponentProps } from 'react-router';
import { Button, Divider } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { CreateOrEditTaxForm } from './Form';
import { TaxTable } from './Table';
import { CustomBreadCrumb } from '../../../components';

interface IProps extends RouteComponentProps, ITaxProps {}

export const TaxPage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.fetchTaxes();
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb title="Quản lý thuế" path="/" />
			<div className="box">
				<div className="box_content">
					<Button
						type="primary"
						icon={<PlusOutlined />}
						onClick={() =>
							props.actions.toggleModal({
								type: TAX_MODAL.ADD_EDIT_TAX,
							})
						}
					>
						Thêm thuế
					</Button>
					<Divider />
					<TaxTable {...props} />
				</div>
			</div>
			<CreateOrEditTaxForm {...props} />
		</React.Fragment>
	);
};
