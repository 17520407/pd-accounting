import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { ITaxProps } from '../../model/ITaxProps';
import { ITaxRecord } from '../../model/ITaxState';
import { IColumn } from '../../../../common/interfaces';
import { useMediaQuery } from 'react-responsive';

const columns = (props: ITaxProps): IColumn[] => {
	return [
		{
			title: '#',
			dataIndex: 'key',
			key: 'key',
			width: 50,
		},
		{
			title: 'Tên thuế',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Phí thuế',
			dataIndex: 'tax',
			key: 'tax',
			render: (text: any, record: ITaxRecord) => <>{text} %</>,
		},
		{
			title: 'Hành động',
			dataIndex: 'operation',
			render: (text: any, record: ITaxRecord) => (
				<Space>
					<Tooltip placement="top" title="Chỉnh sửa">
						<Button
							type="text"
							onClick={() =>
								props.actions.handleCurTax({
									type: 'update',
									tax: record,
								})
							}
							icon={<EditOutlined className="text-primary" />}
						/>
					</Tooltip>
					<Tooltip placement="top" title="Xóa">
						<Popconfirm
							title="Bạn có chắc chắn xoá ngân hàng này?"
							// onConfirm={() => this.handleDelete(record.key)}
							cancelText="Huỷ"
							okText="Xoá"
						>
							<DeleteOutlined className="text-danger" />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

interface IProps extends ITaxProps {}

export const TaxTable: React.FC<IProps> = (props) => {
	const isTabletPortrait = useMediaQuery({ query: '(max-width: 767px)' });
	const { taxRecords, isLoadingTax, isProcessing } = props.store.TaxPage;

	return (
		<Table
			locale={{
				emptyText: <Empty description="Không có dữ liệu" />,
			}}
			rowKey="_id"
			scroll={{
				x: isTabletPortrait ? 1000 : 0,
			}}
			columns={columns(props)}
			loading={isLoadingTax || isProcessing}
			dataSource={taxRecords}
		/>
	);
};
