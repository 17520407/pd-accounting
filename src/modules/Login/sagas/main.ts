import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';

// Handle Admin Login
function* handleAdminLogin(action: any) {
	try {
		const res = yield call(AuthApi.adminLogin, action.payload);
		yield delay(500);
		if (res.status === 200) {
			const { accessToken, refreshToken } = res.data.data;
			localStorage.setItem('accessToken', accessToken);
			localStorage.setItem('refreshToken', refreshToken);
			message.success('Đăng nhập thành công', 1);
			yield put(actions.adminLoginSuccess(res.data.data));
		} else {
			const { errors } = res.data.data;
			message.error(errors[0].error, 2);
			yield put(actions.adminLoginFail(errors));
		}
	} catch (error) {
		message.error('Không có kết nối');
		yield put(actions.adminLoginFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchAdminLogin() {
	yield takeEvery(Keys.ADMIN_LOGIN, handleAdminLogin);
}
/*-----------------------------------------------------------------*/
const sagas = [watchAdminLogin];
export default sagas;
