/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IGetLocations
	| IActions.IGetLocationsSuccess
	| IActions.IGetLocationsFail
	| IActions.ICreateCompany
	| IActions.ICreateCompanySuccess
	| IActions.ICreateCompanyFail
	| IActions.IFetchCompany
	| IActions.IFetchCompanySuccess
	| IActions.IFetchCompanyFail
	| IActions.IFetchCompanyTypes
	| IActions.IFetchCompanyTypesSuccess
	| IActions.IFetchCompanyTypesFail;

export default ActionTypes;
