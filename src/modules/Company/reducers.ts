import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { searchTreeByProperty } from './../../services/searchTree';
import { ICompanyState, COMPANY_MODAL, initialState } from './model/ICompanyState';
import { showErrorMessage } from '../../services/showErrorMessage';
import { errorRoleMessages } from './../../common/constants';

export const name = 'CompanyPage';

export const reducer: Reducer<ICompanyState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.FETCH_COMPANY:
			return onFetchCompany(state, action);
		case Keys.FETCH_COMPANY_SUCCESS:
			return onFetchCompanySuccess(state, action);
		case Keys.FETCH_COMPANY_FAIL:
			return onFetchCompanyFail(state, action);

		case Keys.FETCH_COMPANY_TYPES:
			return onFetchCompanyTypes(state, action);
		case Keys.FETCH_COMPANY_TYPES_SUCCESS:
			return onFetchCompanyTypesSuccess(state, action);
		case Keys.FETCH_COMPANY_TYPES_FAIL:
			return onFetchCompanyTypesFail(state, action);

		case Keys.GET_LOCATIONS:
			return onGetLocations(state, action);
		case Keys.GET_LOCATIONS_SUCCESS:
			return onGetLocationsSuccess(state, action);
		case Keys.GET_LOCATIONS_FAIL:
			return onGetLocationsFail(state, action);

		case Keys.CREATE_COMPANY:
			return onCreateCompany(state, action);
		case Keys.CREATE_COMPANY_SUCCESS:
			return onCreateCompanySuccess(state, action);
		case Keys.CREATE_COMPANY_FAIL:
			return onCreateCompanyFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onToggleModal = (state: ICompanyState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case COMPANY_MODAL.CREATE_EDIT_COMPANY:
			return {
				...state,
				toggleModalCreateEditCompany: !state.toggleModalCreateEditCompany,
			};

		default:
			return {
				...state,
			};
	}
};

const onFetchCompany = (state: ICompanyState, action: IActions.IFetchCompany) => {
	return {
		...state,
		isLoadingCompanyType: true,
	};
};
const onFetchCompanySuccess = (state: ICompanyState, action: IActions.IFetchCompanySuccess) => {
	return {
		...state,
	};
};
const onFetchCompanyFail = (state: ICompanyState, action: IActions.IFetchCompanyFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onFetchCompanyTypes = (state: ICompanyState, action: IActions.IFetchCompanyTypes) => {
	return {
		...state,
		isLoadingCompanyType: true,
	};
};
const onFetchCompanyTypesSuccess = (
	state: ICompanyState,
	action: IActions.IFetchCompanyTypesSuccess
) => {
	return {
		...state,
		isLoadingCompanyType: false,
		companyTypeRecords: action.payload,
	};
};
const onFetchCompanyTypesFail = (state: ICompanyState, action: IActions.IFetchCompanyTypesFail) => {
	return {
		...state,
		isLoadingCompanyType: false,
	};
};

const onGetLocations = (state: ICompanyState, action: IActions.IGetLocations) => {
	return {
		...state,
	};
};
const onGetLocationsSuccess = (state: ICompanyState, action: IActions.IGetLocationsSuccess) => {
	const { data, preLoad } = action.payload;
	const resData = data;
	let provinceRecords = [...state.provinceRecords];
	if (resData[0].level < 3) {
		resData.forEach((record) => {
			record['isLeaf'] = false;
			record['key'] = record['code'].toString();
		});
	} else {
		resData.forEach((record) => {
			record['key'] = record['code'].toString();
		});
	}

	if (provinceRecords.length < 1) {
		provinceRecords = resData;
	} else {
		let result = null;
		provinceRecords.forEach((record) => {
			result = searchTreeByProperty(record, resData[0].parentCode, 'code');
			if (result) {
				result['children'] = resData;
			}
		});
	}

	if (preLoad) {
		return {
			...state,
			provinceRecords,
			isLocationsLoaded: true,
		};
	}
	return {
		...state,
		provinceRecords,
	};
};
const onGetLocationsFail = (state: ICompanyState, action: IActions.IGetLocationsFail) => {
	return {
		...state,
	};
};

const onCreateCompany = (state: ICompanyState, action: IActions.ICreateCompany) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateCompanySuccess = (state: ICompanyState, action: IActions.ICreateCompanySuccess) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onCreateCompanyFail = (state: ICompanyState, action: IActions.ICreateCompanyFail) => {
	showErrorMessage(errorRoleMessages, action.payload.errors[0].code);
	return {
		...state,
		isProcessing: false,
	};
};
