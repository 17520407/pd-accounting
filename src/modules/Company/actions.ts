import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { ICompany, COMPANY_MODAL, IProvince } from './model/ICompanyState';

export const toggleModal = (data: { type: COMPANY_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			...data,
		},
	};
};

//#region GET Locations Actions
export const getLocations = (data: {
	level: number;
	parentCode: number | string;
	preLoad?: boolean; // This option to know which action is latest
}): IActions.IGetLocations => {
	return {
		type: Keys.GET_LOCATIONS,
		payload: {
			...data,
		},
	};
};
export const getLocationsSuccess = (res: {
	preLoad: boolean;
	data: IProvince[];
}): IActions.IGetLocationsSuccess => {
	return {
		type: Keys.GET_LOCATIONS_SUCCESS,
		payload: res,
	};
};
export const getLocationsFail = (res: any): IActions.IGetLocationsFail => {
	return {
		type: Keys.GET_LOCATIONS_FAIL,
		payload: res,
	};
};
//#endregion

//#region Fetch Company Types Actions
export const fetchCompanyTypes = (data: {
	page: number;
	limit: number;
}): IActions.IFetchCompanyTypes => {
	return {
		type: Keys.FETCH_COMPANY_TYPES,
		payload: data,
	};
};

export const fetchCompanyTypesSuccess = (res: any): IActions.IFetchCompanyTypesSuccess => {
	return {
		type: Keys.FETCH_COMPANY_TYPES_SUCCESS,
		payload: res,
	};
};

export const fetchCompanyTypesFail = (res: IError[]): IActions.IFetchCompanyTypesFail => {
	return {
		type: Keys.FETCH_COMPANY_TYPES_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Fetch Company Actions
export const fetchCompany = (data: { page: number; limit: number }): IActions.IFetchCompany => {
	return {
		type: Keys.FETCH_COMPANY,
		payload: data,
	};
};

export const fetchCompanySuccess = (res: any): IActions.IFetchCompanySuccess => {
	return {
		type: Keys.FETCH_COMPANY_SUCCESS,
		payload: res,
	};
};

export const fetchCompanyFail = (res: IError[]): IActions.IFetchCompanyFail => {
	return {
		type: Keys.FETCH_COMPANY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Create Company Actions
export const createCompany = (data: ICompany): IActions.ICreateCompany => {
	return {
		type: Keys.CREATE_COMPANY,
		payload: data,
	};
};

export const createCompanySuccess = (res: any): IActions.ICreateCompanySuccess => {
	return {
		type: Keys.CREATE_COMPANY_SUCCESS,
		payload: res,
	};
};

export const createCompanyFail = (res: IError[]): IActions.ICreateCompanyFail => {
	return {
		type: Keys.CREATE_COMPANY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
