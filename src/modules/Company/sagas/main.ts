import { call, put, takeEvery } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as CompanyApi from '../../../api/company';
import * as ProvinceApi from '../../../api/province';

// Handle GET Locations
function* handleGetLocations(action: any) {
	try {
		const res = yield call(ProvinceApi.getLocations, action.payload);
		if (res.status === 200) {
			yield put(
				actions.getLocationsSuccess({
					data: res.data.data,
					preLoad: action.payload.preLoad,
				})
			);
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.getLocationsFail(error));
	}
}

// Handle Fetch Company Types
function* handleFetchCompanyTypes(action: any) {
	try {
		const res = yield call(CompanyApi.fetchCompanyTypes, action.payload);
		if (res.status === 200) {
			yield put(actions.fetchCompanyTypesSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchCompanyTypesFail(error));
	}
}

// Handle Create Company
function* handleCreateCompany(action: any) {
	try {
		const res = yield call(CompanyApi.createCompany, action.payload);
		if (res.status === 200) {
			yield put(actions.createCompanySuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.createCompanyFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetLocations() {
	yield takeEvery(Keys.GET_LOCATIONS, handleGetLocations);
}
function* watchFetchCompanyTypes() {
	yield takeEvery(Keys.FETCH_COMPANY_TYPES, handleFetchCompanyTypes);
}
function* watchCreateCompany() {
	yield takeEvery(Keys.CREATE_COMPANY, handleCreateCompany);
}
/*-----------------------------------------------------------------*/
const sagas = [watchGetLocations, watchFetchCompanyTypes, watchCreateCompany];
export default sagas;
