export enum COMPANY_MODAL {
	CREATE_EDIT_COMPANY = 0,
}

export interface ICompany {
	companyTypeId: string;
	name: string;
	taxCode: string;
	phone: string;
	address: {
		home: string;
		ward: number;
		district: number;
		city: number;
	};
}

export interface ICompanyType {
	name?: string;
	_id?: string;
}

export interface IProvince {
	title?: string;
	code: number;
	level: number;
	prefix: string;
	name: string;
	fullName: string;
	key?: string;
	isLeaf?: boolean;
	parentCode?: number;
	children?: IProvince[];
}

export interface ICompanyState {
	toggleModalCreateEditCompany: boolean;
	companyRecords: ICompany[];
	companyTypeRecords: ICompanyType[];
	provinceRecords: IProvince[];
	isProcessing: boolean;
	isLoading: boolean;
	isLoadingCompanyType: boolean;
}

// InitialState
export const initialState: ICompanyState = {
	toggleModalCreateEditCompany: false,
	provinceRecords: [],
	companyTypeRecords: [],
	companyRecords: [],
	isLoading: false,
	isLoadingCompanyType: false,
	isProcessing: false,
};
