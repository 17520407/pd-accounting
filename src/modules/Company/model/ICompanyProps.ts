import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ICompanyProps {
	store: IStore;
	actions: typeof Actions;
}
