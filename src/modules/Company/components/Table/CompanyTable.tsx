import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { ICompanyProps } from '../../model/ICompanyProps';
import { IColumn } from '../../../../common/interfaces';
import { useMediaQuery } from 'react-responsive';
import { ICompany } from '../../model/ICompanyState';

const columns = (props: ICompanyProps): IColumn[] => {
	return [
		{
			title: '#',
			dataIndex: 'key',
			key: 'key',
			width: 50,
		},
		{
			title: 'Tên công ty',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Mã số thuế',
			dataIndex: 'taxCode',
			key: 'taxCode',
		},
		{
			title: 'Địa chỉ',
			dataIndex: 'taxCode',
			key: 'taxCode',
		},
		{
			title: 'Hành động',
			dataIndex: 'operation',
			render: (text: any, record: ICompany) => (
				<Space>
					<Tooltip placement="top" title="Chỉnh sửa">
						<Button type="text" icon={<EditOutlined className="text-primary" />} />
					</Tooltip>
					<Tooltip placement="top" title="Xóa">
						<Popconfirm
							title="Bạn có chắc chắn xoá công ty này?"
							// onConfirm={() => this.handleDelete(record.key)}
							cancelText="Huỷ"
							okText="Xoá"
						>
							<DeleteOutlined className="text-danger" />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

interface IProps extends ICompanyProps {}

export const CompanyTable: React.FC<IProps> = (props) => {
	const isTabletPortrait = useMediaQuery({ query: '(max-width: 767px)' });
	const { companyRecords, isLoading, isProcessing } = props.store.CompanyPage;

	return (
		<Table
			locale={{
				emptyText: <Empty description="Không có dữ liệu" />,
			}}
			rowKey="_id"
			scroll={{
				x: isTabletPortrait ? 1000 : 0,
			}}
			columns={columns(props)}
			loading={isLoading || isProcessing}
			dataSource={companyRecords}
		/>
	);
};
