import React from 'react';
import { Input, Button, Typography, Empty, Modal, Cascader, Select, Form, Space } from 'antd';
import { RightOutlined } from '@ant-design/icons';
import { ICompanyProps } from '../../model/ICompanyProps';
import { COMPANY_MODAL } from '../../model/ICompanyState';

const { Option } = Select;
const { Title } = Typography;

interface IProps extends ICompanyProps {}

interface IInputs {
	name: string;
	companyTypeId: string;
	province: number[];
	home: string;
	phone: string;
	taxCode: string;
}

export const CreateOrEditCompanyForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const {
		isProcessing,
		isLoadingCompanyType,
		toggleModalCreateEditCompany,
		provinceRecords,
		companyTypeRecords,
	} = props.store.CompanyPage;
	const onFinish = (data: IInputs) => {
		const { name, companyTypeId, phone, province, home, taxCode } = data;
		props.actions.createCompany({
			name,
			companyTypeId,
			taxCode,
			phone,
			address: {
				home,
				city: province[0],
				district: province[1],
				ward: province[2],
			},
		});
	};

	const loadData = (selectedOptions: any) => {
		const targetOption = selectedOptions[selectedOptions.length - 1];
		props.actions.getLocations({
			level: targetOption.level + 1,
			parentCode: targetOption.code,
		});
	};

	const selectProps = {
		showSearch: true,
		disabled: isProcessing,

		style: { width: '100%' },
		placeholder: 'Tìm kiếm vai trò',
		optionLabelProp: 'label',
		optionFilterProp: 'children',
		notFoundContent: <Empty description="Không có dữ liệu" />,
		filterOption: (input: any, option: any) =>
			option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0,
	};

	return (
		<Modal
			visible={toggleModalCreateEditCompany}
			onCancel={() => {
				formInstance.resetFields();
				// props.actions.handleClear(EMPLOYEE_CLEAR.EMPLOYEE_UPDATE);
				props.actions.toggleModal({
					type: COMPANY_MODAL.CREATE_EDIT_COMPANY,
				});
			}}
			centered={true}
			afterClose={() => {
				formInstance.resetFields();
			}}
			maskClosable={false}
			footer={null}
		>
			<Title level={4}>Tạo mới công ty</Title>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Form.Item
					label="Tên công ty"
					name="name"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={
						<Input placeholder="Nhâp tên công ty" allowClear={true} disabled={isProcessing} />
					}
				/>
				<Form.Item
					label="Loại công ty"
					name="companyTypeId"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={
						<Select {...selectProps} loading={isLoadingCompanyType}>
							{companyTypeRecords &&
								companyTypeRecords.map((item) => {
									return (
										<Option key={item._id} value={item._id as string} label={item.name}>
											{item.name}
										</Option>
									);
								})}
						</Select>
					}
				/>
				<Form.Item
					label="Tinh/thành phố - Quận/Huyện - Phường/xã"
					name="province"
					rules={[
						{
							required: true,
						},
					]}
				>
					<Cascader
						expandIcon={<RightOutlined />}
						fieldNames={{
							label: 'fullName',
							value: 'code',
							children: 'children',
						}}
						options={provinceRecords}
						loadData={loadData}
						placeholder="Địa chỉ"
						disabled={isProcessing}
					/>
				</Form.Item>

				<Form.Item
					label="Địa chỉ nhà, đường"
					name="home"
					rules={[
						{
							required: true,
						},
						{
							max: 100,
						},
					]}
				>
					<Input placeholder="Nhập địa chỉ thường trú" disabled={isProcessing} />
				</Form.Item>
				<Form.Item
					label="Mã thuế"
					name="taxCode"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={<Input placeholder="Nhâp mã thuế" allowClear={true} disabled={isProcessing} />}
				/>
				<Form.Item
					label="Điện thoại"
					name="phone"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={
						<Input placeholder="Nhâp điện thoại" allowClear={true} disabled={isProcessing} />
					}
				/>

				<div className="d-flex w-100 justify-content-end">
					<Space>
						<Button
							className="text-capitalize"
							onClick={() => {
								props.actions.toggleModal({
									type: COMPANY_MODAL.CREATE_EDIT_COMPANY,
								});
							}}
						>
							Đóng
						</Button>
						<Button
							className="text-capitalize"
							type="primary"
							htmlType="submit"
							loading={isProcessing}
						>
							Tạo
						</Button>
					</Space>
				</div>
			</Form>
		</Modal>
	);
};
