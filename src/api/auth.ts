import { request } from '../config/axios';

export const adminLogin = (data: { username: string; password: string }) => {
	const endpoint = '/admin/login';
	return request(endpoint, 'POST', data);
};
