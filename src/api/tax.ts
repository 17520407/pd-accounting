import { request } from '../config/axios';

export const fetchTaxes = (data: { page: number; limit: number }) => {
	const endpoint = `/settingTax`;
	return request(endpoint, 'GET', null);
};

export const createTax = (data: any) => {
	const endpoint = `/settingTax`;
	return request(endpoint, 'POST', data);
};

export const updateTax = (data: { _id: string; tax: any }) => {
	const endpoint = `/settingTax/${data._id}`;
	return request(endpoint, 'PUT', data.tax);
};

export const deleteTax = (data: { _id: string }) => {
	const endpoint = `/settingTax/${data._id}`;
	return request(endpoint, 'DELETE', null);
};
