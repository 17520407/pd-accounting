import { request } from '../config/axios';

export const fetchCurrencyType = (data: { page: number; limit: number }) => {
	const endpoint = `/moneyType?page=${data.page}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const createCurrencyType = (data: any) => {
	const endpoint = `/moneyType`;
	return request(endpoint, 'POST', data);
};

export const updateCurrencyType = (data: { _id: string }) => {
	const endpoint = `/moneyType/${data._id}`;
	return request(endpoint, 'PUT', data);
};

export const deleteCurrencyType = (data: { _id: string }) => {
	const endpoint = `/moneyType/${data._id}`;
	return request(endpoint, 'DELETE', data);
};
