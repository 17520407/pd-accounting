import { request } from '../config/axios';

export const getRoles = (data: { page: number; limit: number }) => {
	const endpoint = `/adminRoles?page=${data.page}&&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const postRole = (data: { roleName: string }) => {
	const endpoint = '/adminRoles';
	return request(endpoint, 'POST', data);
};

export const putRole = (data: { _id: string; roleName: string; rolePermissions: any[] }) => {
	const endpoint = `/adminRoles/${data._id}`;
	return request(endpoint, 'PUT', {
		roleName: data.roleName,
		rolePermissions: data.rolePermissions,
	});
};

export const deleteRole = (data: { _id: string }) => {
	const endpoint = `/adminRoles/${data._id}`;
	return request(endpoint, 'DELETE', null);
};

export const enableRole = (data: { _id: string }) => {
	const endpoint = `/adminRoles/enable/${data._id}`;
	return request(endpoint, 'PUT', null);
};
export const disableRole = (data: { _id: string }) => {
	const endpoint = `/adminRoles/disable/${data._id}`;
	return request(endpoint, 'PUT', null);
};

export const getPermissions = () => {
	const endpoint = `/adminPermissions`;
	return request(endpoint, 'GET', null);
};
