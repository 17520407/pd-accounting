import { request } from '../config/axios';

export const uploadImage = (data: File) => {
	const endpoint = '/files/images';
	const formData = new FormData();
	formData.append('file', data);

	return request(endpoint, 'POST', formData);
};
