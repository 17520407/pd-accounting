import { request } from '../config/axios';
import { IAccountNumber } from '../modules/AccountNumber/model/IAccountNumberState';

export const fetchAccountNumber = (data: { page: number; limit: number }) => {
	const endpoint = `/accountNumber?page=${data.page}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const createAccountNumber = (data: any) => {
	const endpoint = `/accountNumber`;
	return request(endpoint, 'POST', data);
};

export const updateAccountNumber = (data: {
	_id: string;
	data: { circulars: string; accountNumbers: IAccountNumber[] };
}) => {
	const endpoint = `/accountNumber/${data._id}`;
	return request(endpoint, 'PUT', data.data);
};

export const deleteAccountNumber = (data: { _id: string }) => {
	const endpoint = `/accountNumber/${data._id}`;
	return request(endpoint, 'DELETE', data);
};
