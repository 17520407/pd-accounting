import { request } from '../config/axios';
import { IBankAccount } from '../modules/BankAccount/model/IBankAccountState';

export const getAllBanks = () => {
	const endpoint = '/settingBank';
	return request(endpoint, 'GET', null);
};

export const postBank = (data: { bankInfo: any }) => {
	const endpoint = '/settingBank';
	return request(endpoint, 'POST', data.bankInfo);
};

export const fetchAllBankAccount = () => {
	const endpoint = '/settingBank';
	return request(endpoint, 'GET', null);
};

export const createBankAccount = (data: any) => {
	const endpoint = '/settingBank';
	return request(endpoint, 'POST', data);
};

export const updateBankAccount = (data: { _id: string; bankAccount: IBankAccount }) => {
	const endpoint = `/settingBank/${data._id}`;
	return request(endpoint, 'PUT', data.bankAccount);
};

export const deleteBankAccount = (data: { _id: string }) => {
	const endpoint = `/settingBank/${data._id}`;
	return request(endpoint, 'DELETE', null);
};
