import { request } from '../config/axios';
import { ISystemConstant } from '../modules/SystemConstant/model/ISystemConstantState';

export const fetchSystemConst = () => {
	const endpoint = `/systemConst`;
	return request(endpoint, 'GET', null);
};

export const updateSystemConst = (data: ISystemConstant) => {
	const endpoint = `/systemConst`;
	return request(endpoint, 'PUT', data);
};
