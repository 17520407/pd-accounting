import { request } from '../config/axios';

export const fetchUserRoles = (data: { page: string; limit: string }) => {
	const endpoint = `/userRoles?page=${data.page}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const createUserRole = (data: any) => {
	const endpoint = `/userRoles`;
	return request(endpoint, 'POST', data);
};

export const updateUserRole = (data: { _id: string }) => {
	const endpoint = `/userRoles/${data._id}`;
	return request(endpoint, 'PUT', data);
};

export const deleteUserRole = (data: any) => {
	const endpoint = `/userRoles`;
	return request(endpoint, 'DELETE', data);
};

export const disableUserRole = (data: { _id: string }) => {
	const endpoint = `/userRoles/${data._id}`;
	return request(endpoint, 'PUT', data);
};

export const enableUserRole = (data: { _id: string }) => {
	const endpoint = `/userRoles/${data._id}`;
	return request(endpoint, 'PUT', data);
};
