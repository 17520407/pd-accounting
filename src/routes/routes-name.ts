export const routeName = {
	login: '/login',
	rolePermission: '/role-permission',
	users: '/user',
	systemConstant: '/system-constant',
	company: '/management/company',
	currency: '/management/currency',
	accountNumber: '/management/account-number',
	companyType: '/management/company-type',
	bank: '/management/bank',
	tax: '/management/tax',
	notAccept: '/not-accept',
};
