import React, { ComponentClass, FunctionComponent, LazyExoticComponent } from 'react';
import { routeName } from './routes-name';
import { NotAcceptPage } from '../components/Pages';

const LoginPage = React.lazy(() => import('../modules/Login/components/LoginPageContainer'));
const PermissionPage = React.lazy(
	() => import('../modules/Permission/components/PermissionPageContainer')
);
const AccountNumberPage = React.lazy(
	() => import('../modules/AccountNumber/components/AccountNumberPageContainer')
);
const CompanyTypePage = React.lazy(
	() => import('../modules/CompanyType/components/CompanyTypePageContainer')
);
const SystemConstantPage = React.lazy(
	() => import('../modules/SystemConstant/components/SystemConstantPageContainer')
);
const CurrencyPage = React.lazy(
	() => import('../modules/Currency/components/CurrencyPageContainer')
);
const BankAccountPage = React.lazy(
	() => import('../modules/BankAccount/components/BankAccountPageContainer')
);
const TaxPage = React.lazy(() => import('../modules/Tax/components/TaxPageContainer'));

export interface RouteConfig {
	path: string;
	extract: boolean;
	component: ComponentClass | FunctionComponent | LazyExoticComponent<any>;
	permission?: string;
}

export const authRoutes: RouteConfig[] = [
	{
		path: routeName.login,
		extract: true,
		component: LoginPage,
	},
];

export const mainRoutes: RouteConfig[] = [
	{
		path: routeName.companyType,
		extract: true,
		component: CompanyTypePage,
	},
	{
		path: routeName.systemConstant,
		extract: true,
		component: SystemConstantPage,
	},
	{
		path: routeName.currency,
		extract: true,
		component: CurrencyPage,
	},
	{
		path: routeName.accountNumber,
		extract: true,
		component: AccountNumberPage,
	},
	{
		path: routeName.bank,
		extract: true,
		component: BankAccountPage,
	},
	{
		path: routeName.tax,
		extract: true,
		component: TaxPage,
	},
	{
		path: routeName.rolePermission,
		extract: true,
		component: PermissionPage,
	},
];

export const otherRoutes: RouteConfig[] = [
	{
		path: routeName.notAccept,
		extract: true,
		component: NotAcceptPage,
	},
];
