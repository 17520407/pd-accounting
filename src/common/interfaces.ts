import { CompareFn, ColumnType } from 'antd/lib/table/interface';

export enum PERMISSION_KEY {}

export enum ErrorsCode {
	invalid = 'invalid',
	serverError = 'serverError',

	// validate
	validateUserBorrowerHasBanking = 'validateUserBorrowerHasBanking',

	// userError
	userPhoneRegistered = 'userPhoneRegistered',
	userNotFound = 'userNotFound',
	userNotActivated = 'userNotActivated',
	userWrongPassword = 'userWrongPassword',
	userCanNotReject = 'userCanNotReject',
	userCanNotApprove = 'userCanNotApprove',
	userNotVerifyKyc = 'userNotVerifyKyc',
	// verify
	verifyCodeNotFound = 'verifyCodeNotFound',
	verifyUserHasActivated = 'verifyUserHasActivated',
	verifyCodeHasSent = 'verifyCodeHasSent',
	verifyCantForgotPassword = 'verifyCantForgotPassword',
	verifyCantRegister = 'verifyCantRegister',

	// userBorrowerBanking
	userBorrowerBankingNotFound = 'userBankingNotFound',

	// files
	filesOnlyAcceptImageFiles = 'filesOnlyAcceptImageFiles',

	// companyBanking
	companyBankingExisted = 'companyBankingExisted',
	companyBankingNotExist = 'companyBankingNotExist',
	companyBankingDefaultNotExisted = 'companyBankingDefaultNotExisted',

	// userBorrowerDeposit
	userBorrowerDepositWaitingOrRejectExisted = 'userBorrowerDepositWaitingOrRejectExisted',
	userBorrowerDepositNotExist = 'userBorrowerDepositNotExist',
	userBorrowerDepositWaitingOrRejectNotExisted = 'userBorrowerDepositWaitingOrRejectNotExisted',
	userBorrowerDepositCanNotApprove = 'userBorrowerCanNotApprove',
	userBorrowerDepositCanOnlyRejectWaiting = 'userBorrowerDepositCanOnlyRejectWaiting',

	// configBanks
	configBanksExisted = 'configBanksExisted',
}

export interface IPagination {
	totalPage: number;
	totalRecord: number;
}

export interface IError {
	code: string;
	message?: string;
	error?: string;
}

export interface IPermission {
	_id: string;
	permissionKey: string;
}

export interface IPermissionObj {
	key: string;
	title: string;
	dependOn: { name: string; key: string }[];
	value: string;
	isActive?: boolean;
	_id: string;
	children?: IPermissionObj[];
}

export interface IRole {
	_id?: string;
	roleName: string;
	rolePermissions: { permissionKey: string; _id?: string }[];
	status?: string;
}

export interface IColumn extends ColumnType<any> {
	title: string;
	dataIndex?: string;
	key?: string;
	defaultSortOrder?: any;
	sorter?: CompareFn<any>;
	shouldCellUpdate?: (record: any, prevRecord: any) => boolean;
	render?: any;
	colSpan?: number;
}

export interface IValidateMessage {
	[x: string]: string | object;
}
